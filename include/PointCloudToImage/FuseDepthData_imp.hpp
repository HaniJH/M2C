
#ifndef _FUSE_DEPTH_DATA_IMP_20150806_HPP_
#define _FUSE_DEPTH_DATA_IMP_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// forward declaration
class FuseDepthData;

//==============================================================================
//==============================================================================

template<class Archive>
void
FuseDepthData::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( flag_criterion_factor );
    ar & BOOST_SERIALIZATION_NVP( tolerance );
    ar & BOOST_SERIALIZATION_NVP( plus_neighbours_weight );
    ar & BOOST_SERIALIZATION_NVP( cross_neighbours_weight );
    ar & BOOST_SERIALIZATION_NVP( direct_pixel_weight );
    ar & BOOST_SERIALIZATION_NVP( flag_affect_neighbours );
    ar & BOOST_SERIALIZATION_NVP( flag_only_empty_neighbours );
}

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
