
#ifndef _POINT_CLOUD_TO_PYRAMID_LOG_IMP_20150806_HPP_
#define _POINT_CLOUD_TO_PYRAMID_LOG_IMP_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// forward declaration
class PointCloudToPyramidLog;

//==============================================================================
//==============================================================================

template<class Archive>
void
PointCloudToPyramidLog::serialize( Archive & ar, const unsigned int version )
{
//    ar & BOOST_SERIALIZATION_NVP( version );
    ar & BOOST_SERIALIZATION_NVP( color_image_filename );
    ar & BOOST_SERIALIZATION_NVP( depth_image_filename );
    ar & BOOST_SERIALIZATION_NVP( original_cloud_filename );
    ar & BOOST_SERIALIZATION_NVP( voxelized_cloud_filename );
    ar & BOOST_SERIALIZATION_NVP( horizontal_FOV );
    ar & BOOST_SERIALIZATION_NVP( vertical_FOV );
    ar & BOOST_SERIALIZATION_NVP( pose );
}

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
