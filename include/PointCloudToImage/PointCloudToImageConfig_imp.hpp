
#ifndef _POINT_CLOUD_TO_IMAGE_CONFIG_IMP_20150806_HPP_
#define _POINT_CLOUD_TO_IMAGE_CONFIG_IMP_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// forward declaration
class PointCloudToImageConfig;

//==============================================================================
//==============================================================================

template<class Archive>
void
PointCloudToImageConfig::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( horizontal_FOV );
    ar & BOOST_SERIALIZATION_NVP( vertical_FOV );
    ar & BOOST_SERIALIZATION_NVP( voxel_size );
    ar & BOOST_SERIALIZATION_NVP( axis_toward_scene );
    ar & BOOST_SERIALIZATION_NVP( desired_image_size_rows );
    ar & BOOST_SERIALIZATION_NVP( desired_image_size_cols );
    ar & BOOST_SERIALIZATION_NVP( fuse_color_data );
    ar & BOOST_SERIALIZATION_NVP( fuse_depth_data );
}

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
