
#ifndef POINT_CLOUD_PARSING_CONFIG_IMP_20150806_HPP_
#define POINT_CLOUD_PARSING_CONFIG_IMP_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// forward declaration
class PointCloudParsingConfig;

//==============================================================================
//==============================================================================

template<class Archive>
void
PointCloudParsingConfig::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( pcd_to_img_cfg );
    ar & BOOST_SERIALIZATION_NVP( horizontal_overlap );
    ar & BOOST_SERIALIZATION_NVP( vertical_overlap );
    ar & BOOST_SERIALIZATION_NVP( max_horizontal_FOV );
    ar & BOOST_SERIALIZATION_NVP( min_horizontal_FOV );
    ar & BOOST_SERIALIZATION_NVP( max_vertical_FOV );
    ar & BOOST_SERIALIZATION_NVP( min_vertical_FOV );
    ar & BOOST_SERIALIZATION_NVP( virtual_camera_pose );
}

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
