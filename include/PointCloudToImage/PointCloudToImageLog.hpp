#ifndef _POINT_CLOUD_TO_IMAGE_LOG_20152806_HPP_
#define _POINT_CLOUD_TO_IMAGE_LOG_20152806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "haniDefinitions.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

//==============================================================================
//==============================================================================

/*!
 * \brief The PointCloudToImageLog class encapsulates the information to map
 * an image to its pose.
 */
class PointCloudToImageLog
{
public:
    PointCloudToImageLog();
    //!< Default constructor.

    PointCloudToImageLog( std::string filename_, double hfov_
                          , double vfov_, hjh::Pose pose_ );
    //!< Constructor.

    virtual
    ~PointCloudToImageLog();
    //!< Destructor.


    typedef boost::shared_ptr<PointCloudToImageLog> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const PointCloudToImageLog> ConstPtr;
    //!< Const shared pointer.


    friend std::ostream&
    operator<<(std::ostream& os, const PointCloudToImageLog& obj);
    //!< Overloaded output-stream.


private:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler.

public:

    std::string filename;
    //!< The filename for the extracted image.

    double horizontal_FOV;
    //!< Horizontal field of view in degrees.

    double vertical_FOV;
    //!< Vetical field of view in degrees.

    hjh::Pose pose;
    //!< The pose of the virtual camera in the pointcloud framework to extract the image.
};

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/PointCloudToImageLog_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
