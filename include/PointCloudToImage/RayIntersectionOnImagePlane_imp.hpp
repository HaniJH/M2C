

#ifndef RAY_INTERSECTION_ON_IMAGE_PLANE_IMP_20150806_HPP_
#define RAY_INTERSECTION_ON_IMAGE_PLANE_IMP_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// forward declaration
class RayIntersectionOnImagePlane;

//==============================================================================
//==============================================================================

template<class Archive>
void
RayIntersectionOnImagePlane::serialize( Archive & ar, const unsigned int version )
{
    ar & BOOST_SERIALIZATION_NVP( f );
    ar & BOOST_SERIALIZATION_NVP( col );
    ar & BOOST_SERIALIZATION_NVP( row );
    ar & BOOST_SERIALIZATION_NVP( d );
    ar & BOOST_SERIALIZATION_NVP( z );
    ar & BOOST_SERIALIZATION_NVP( z_scale );
    ar & BOOST_SERIALIZATION_NVP( r );
    ar & BOOST_SERIALIZATION_NVP( g );
    ar & BOOST_SERIALIZATION_NVP( b );
    ar & BOOST_SERIALIZATION_NVP( Phi );
}

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#endif

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
