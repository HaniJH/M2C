#ifndef _POINT_CLOUD_TO_PYRAMID_LOG_20150806_HPP_
#define _POINT_CLOUD_TO_PYRAMID_LOG_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "haniDefinitions.hpp"
#include "PointCloudToImage/PointCloudToImageLog.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

//==============================================================================
//==============================================================================

/*!
 * \brief The PointCloudToPyramidLog class encapsulates the information to map
 * a pyramid to its pose.
 */
class PointCloudToPyramidLog: public PointCloudToImageLog
{
public:

    PointCloudToPyramidLog();
    //!< Default constructor.

    /*!
     * \brief Constructor.
     * \note filename_ should contain only the file name (stem) <i>without any extension</i>.
     */
    PointCloudToPyramidLog( std::string filename_, double hfov_
                          , double vfov_, hjh::Pose pose_ );

    virtual
    ~PointCloudToPyramidLog();
    //!< Destructor.


    typedef boost::shared_ptr<PointCloudToPyramidLog> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const PointCloudToPyramidLog> ConstPtr;
    //!< Const shared pointer.


    friend std::ostream&
    operator<<(std::ostream& os, const PointCloudToPyramidLog& obj);
    //!< Overloaded output-stream.


private:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler.

public:

    std::string color_image_filename;
    //!< The filename for the extracted color image of the pyramid.

    std::string depth_image_filename;
    //!< The filename for the extracted color depth of the pyramid.

    std::string original_cloud_filename;
    //!< The filename for the original point cloud of the pyramid.

    std::string voxelized_cloud_filename;
    //!< The filename for the voxelized point cloud of the pyramid.

};

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/PointCloudToPyramidLog_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
