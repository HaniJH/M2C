

#ifndef _POINTCLOUD_TO_IMAGE_20150612_HPP_
#define _POINTCLOUD_TO_IMAGE_20150612_HPP_
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "FrustumCulling/FrustumCulling.hpp"

#include "PointCloudToImage/FuseColorData.hpp"
#include "PointCloudToImage/FuseDepthData.hpp"
#include "PointCloudToImage/RayIntersectionOnImagePlane.hpp"
#include "PointCloudToImage/PointCloudParsingConfig.hpp"
#include "PointCloudToImage/SquarePyramidPack.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <vector>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

/*!
 * \brief The MODE enum defines two modes for converting a given point cloud to
 * its corresponding 2D image.
 */
enum MODE {
    RAYCASTING_FROM_CAMERA_TO_MODEL = 0
    //!< Raycasting, which means to raycast form the camera center to a voxelized 3D model to obtain the corresponding pixel in 2D image.
    , BACK_RAYCASTING_FROM_MODEL_TO_CAMERA = 1
    //!< Back raycasting, which means to raycast form each 3D point to the camera center and image plane, accordingly. It is needed to handle multiple points that influencing the same pixel in the 2D image.
     };

// *****************************************************************************
// *********************      P R O T O T Y P E S    ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

class PointCloudToImageConfig;
class PointCloudParsingConfig;
class PointCloudToImageLog;
class SquarePyramidPack;

//==============================================================================
//==============================================================================

/*!
 * \brief convert_pointcloud_to_image, it must be obvious form the name what this
 * function does (especifically a PCL PointXYZRGB pointcloud to an OpenCV RGB image).
 *
 * \note <B>Important:</B> it assumes that Z-axis is toward the scene.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud.
 * \param[out]  image           resulting RGB image in OpenCV Mat format.
 * \param[in]   horizontal_fov  horizontal field of view in degrees.
 * \param[in]   vertical_fov    vetical field of view in degrees.
 * \param[in]   voxel_size      resolution of voxel-grid in meters (and resolutin of iamge accordingly).
 * \param[in]   axis_to_scene   indicates the axis pointing to the scene.
 * \param[in]   desired_rows    indicates the desired number of rows in the resulting image.
 * \param[in]   desired_cols    indicates the desired number of columns in the resulting image.
 * \note Zero values for desired_rows or desired_cols means no desired value and
 * the they are automatically being calculated based on FOV and voxel size (default).
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 * \param[in]   flag_criterion_factor_      refer to \ref FuseColorData class.
 * \param[in]   tolerance_                  refer to \ref FuseColorData class.
 * \param[in]   plus_neighbours_weight_     refer to \ref FuseColorData class.
 * \param[in]   cross_neighbours_weight_    refer to \ref FuseColorData class.
 * \param[in]   direct_pixel_weight_        refer to \ref FuseColorData class.
 * \param[in]   flag_affect_neighbours_     refer to \ref FuseColorData class.
 * \param[in]   flag_only_empty_neighbours_ refer to \ref FuseColorData class.
 *
 * \par Steps performed in various modes:
 * - \ref RAYCASTING_FROM_CAMERA_TO_MODEL
 *     -# PCD ---(FOV filtering)---> filtered PCD \n
 *     -# filtered PCD ---(voxelizing)---> voxelized model \n
 *     -# raycast: camera center ---> voxelized model \n
 *     -# calculating pixels values for the resulting image. \n
 *
 * - \ref BACK_RAYCASTING_FROM_MODEL_TO_CAMERA
 *     -# PCD ---(FOV filtering)---> filtered PCD \n
 *     -# back-raycast: filtered PCD ---> camera center \n
 *     -# calculating pixels values for the resulting image. \n
 * \note mutiple points affecting same pixel should be handled for \ref
 * BACK_RAYCASTING_FROM_MODEL_TO_CAMERA mode.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_depth_image and convert_pointcloud_to_frustum_pack.
 *
 */
int
convert_pointcloud_to_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &image
                             , double horizontal_fov
                             , double vertical_fov
                             , double voxel_size
                             , int axis_to_scene
                             , unsigned int desired_rows = 0
                             , unsigned int desired_cols = 0
                             , MODE mode = BACK_RAYCASTING_FROM_MODEL_TO_CAMERA
                             , const FuseColorData &fd = FuseColorData(1,1)
        );

//==============================================================================
//==============================================================================

/*!
 * \brief convert_pointcloud_to_image, it must be obvious form the name what this
 * function does (especifically a PCL PointXYZRGB pointcloud to an OpenCV RGB image).
 *
 * \note this function overloads the \ref convert_pointcloud_to_image.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud.
 * \param[out]  image           resulting RGB image in OpenCV Mat format.
 * \param[in]   cfg             includes configuration data.
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_image and convert_pointcloud_to_frustum_pack.
 */
int
convert_pointcloud_to_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &image
                             , hjh::pcd2img::PointCloudToImageConfig cfg
                             , MODE mode = BACK_RAYCASTING_FROM_MODEL_TO_CAMERA
        );

//==============================================================================
//==============================================================================

/*!
 * \brief convert_pointcloud_to_depth_image, it must be obvious form the name what this
 * function does (especifically a PCL PointXYZ pointcloud to an OpenCV depth image).
 *
 * \note <B>Important:</B> it assumes that Z-axis is toward the scene.
 * \note This method assumes that the input point cloud is in <B>meter</B> and
 * returns the output depth image in <B>millimeter</B>.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud.
 * \param[out]  dimage          resulting depth image in OpenCV Mat format.
 * \param[in]   horizontal_fov  horizontal field of view in degrees.
 * \param[in]   vertical_fov    vetical field of view in degrees.
 * \param[in]   voxel_size      resolution of voxel-grid in meters (and resolutin of iamge accordingly).
 * \param[in]   axis_to_scene   indicates the axis pointing to the scene.
 * \param[in]   desired_rows    indicates the desired number of rows in the resulting image.
 * \param[in]   desired_cols    indicates the desired number of columns in the resulting image.
 * \note Zero values for desired_rows or desired_cols means no desired value and
 * the they are automatically being calculated based on FOV and voxel size (default).
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 *
 * \par Steps performed in various modes:
 * - \ref RAYCASTING_FROM_CAMERA_TO_MODEL
 *     -# PCD ---(FOV filtering)---> filtered PCD \n
 *     -# filtered PCD ---(voxelizing)---> voxelized model \n
 *     -# raycast: camera center ---> voxelized model \n
 *     -# calculating pixels values for the resulting image. \n
 *
 * - \ref BACK_RAYCASTING_FROM_MODEL_TO_CAMERA
 *     -# PCD ---(FOV filtering)---> filtered PCD \n
 *     -# back-raycast: filtered PCD ---> camera center \n
 *     -# calculating pixels values for the resulting image. \n
 * \note mutiple points affecting same pixel should be handled for \ref
 * BACK_RAYCASTING_FROM_MODEL_TO_CAMERA mode.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_depth_image.
 *
 */
int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
                             , cv::Mat &dimage
                             , double horizontal_fov
                             , double vertical_fov
                             , double voxel_size
                             , int axis_to_scene
                             , unsigned int desired_rows = 0
                             , unsigned int desired_cols = 0
                             , MODE mode = RAYCASTING_FROM_CAMERA_TO_MODEL
                             , const FuseDepthData &fd = FuseDepthData(1,1) );


/*!
 * \brief convert_pointcloud_to_depth_image, it must be obvious form the name what this
 * function does (especifically a PCL PointXYZRGB pointcloud to an OpenCV depth image).
 *
 * \note <B>Important:</B> it assumes that Z-axis is toward the scene.
 * \note This method assumes that the input point cloud is in <B>meter</B> and
 * returns the output depth image in <B>millimeter</B>.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud.
 * \param[out]  dimage          resulting depth image in OpenCV Mat format.
 * \param[in]   horizontal_fov  horizontal field of view in degrees.
 * \param[in]   vertical_fov    vetical field of view in degrees.
 * \param[in]   voxel_size      resolution of voxel-grid in meters (and resolutin of iamge accordingly).
 * \param[in]   axis_to_scene   indicates the axis pointing to the scene.
 * \param[in]   desired_rows    indicates the desired number of rows in the resulting image.
 * \param[in]   desired_cols    indicates the desired number of columns in the resulting image.
 * \note Zero values for desired_rows or desired_cols means no desired value and
 * the they are automatically being calculated based on FOV and voxel size (default).
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 *
 * \par Steps performed in various modes:
 * - \ref RAYCASTING_FROM_CAMERA_TO_MODEL
 *     -# PCD ---(FOV filtering)---> filtered PCD \n
 *     -# filtered PCD ---(voxelizing)---> voxelized model \n
 *     -# raycast: camera center ---> voxelized model \n
 *     -# calculating pixels values for the resulting image. \n
 *
 * - \ref BACK_RAYCASTING_FROM_MODEL_TO_CAMERA
 *     -# PCD ---(FOV filtering)---> filtered PCD \n
 *     -# back-raycast: filtered PCD ---> camera center \n
 *     -# calculating pixels values for the resulting image. \n
 * \note mutiple points affecting same pixel should be handled for \ref
 * BACK_RAYCASTING_FROM_MODEL_TO_CAMERA mode.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_depth_image.
 *
 */
int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &dimage
                             , double horizontal_fov
                             , double vertical_fov
                             , double voxel_size
                             , int axis_to_scene
                             , unsigned int desired_rows = 0
                             , unsigned int desired_cols = 0
                             , MODE mode = RAYCASTING_FROM_CAMERA_TO_MODEL
                             , const FuseDepthData &fd = FuseDepthData(1,1) );

//==============================================================================
//==============================================================================

/*!
 * \brief convert_pointcloud_to_depth_image, it must be obvious form the name what this
 * function does (especifically a PCL PointXYZ pointcloud to an OpenCV depth image).
 *
 * \note this function overloads the \ref convert_pointcloud_to_image.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud.
 * \param[out]  dimage          resulting depth image in OpenCV Mat format.
 * \param[in]   cfg             includes configuration data.
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_depth_image and convert_pointcloud_to_frustum_pack.
 */
int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
                             , cv::Mat &dimage
                             , hjh::pcd2img::PointCloudToImageConfig cfg
                             , MODE mode = RAYCASTING_FROM_CAMERA_TO_MODEL );

/*!
 * \brief convert_pointcloud_to_depth_image, it must be obvious form the name what this
 * function does (especifically a PCL PointXYZRGB pointcloud to an OpenCV depth image).
 *
 * \note this function overloads the \ref convert_pointcloud_to_image.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud.
 * \param[out]  dimage          resulting depth image in OpenCV Mat format.
 * \param[in]   cfg             includes configuration data.
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_depth_image and convert_pointcloud_to_frustum_pack.
 */
int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &dimage
                             , hjh::pcd2img::PointCloudToImageConfig cfg
                             , MODE mode = RAYCASTING_FROM_CAMERA_TO_MODEL );

//==============================================================================
//==============================================================================

/*!
 * \brief convert_pointcloud_to_frustum_pack converts an input point cloud
 * (specifically a PCL PointXYZRGB pointcloud) to the corresponding \ref SquarePyramidPack.
 *
 * \param[in]   cloud_ptr       a smart pointer to the source cloud (NOT FOV-filtered).
 * \param[out]  pack            resulting \ref SquarePyramidPack.
 * \param[in]   cfg             includes configuration data.
 * \param[in]   mode            to choose how perform the job based on \ref MODE.
 *
 * \return zero if everything goes well, a negative number otherwise.
 *
 * \sa convert_pointcloud_to_image.
 */
int
convert_pointcloud_to_square_pyramid_pack(
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , hjh::pcd2img::SquarePyramidPack &pack
        , hjh::pcd2img::PointCloudToImageConfig cfg
        , MODE mode = RAYCASTING_FROM_CAMERA_TO_MODEL );

//==============================================================================
//==============================================================================


// *****************************************************************************
// *********************         C L A S S E S       ***************************
// *****************************************************************************

// empty

//==============================================================================
//==============================================================================


// *****************************************************************************
// *****************************************************************************
} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/PointCloudToImage_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
