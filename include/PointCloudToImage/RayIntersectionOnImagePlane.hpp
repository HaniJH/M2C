#ifndef RAY_INTERSECTION_ON_IMAGE_PLANE_20150806_HPP_
#define RAY_INTERSECTION_ON_IMAGE_PLANE_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "haniDefinitions.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

//==============================================================================
//==============================================================================

/*!
 * \brief The RayIntersectionOnImagePlane class encapsulates the data related to
 * intersection of a ray from the source point cloud to the pixel-independent
 * image plane.
 * \note All the units for the data members will be defined by users.
 */
class RayIntersectionOnImagePlane
{
public:

    //--------------------------------------------------------------------------
    RayIntersectionOnImagePlane();
    //! Default constructor.

    /*!
     * \brief RayIntersectionOnImagePlane is the constructor of this class.
     * \param[in]   focal_length_   obviously it is the focal length in any unit
     * desired by the user.
     * \param[in]   axis_to_scene_  defines the axis which is towards the scene
     * (0=X, 1=Y, and 2=Z).
     */
    RayIntersectionOnImagePlane( float focal_length_, int axis_to_scene_ );

    virtual
    ~RayIntersectionOnImagePlane();
    //! Destructor.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    typedef boost::shared_ptr<RayIntersectionOnImagePlane> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const RayIntersectionOnImagePlane> ConstPtr;
    //!< Const shared pointer.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    friend std::ostream&
    operator<<(std::ostream& os, const RayIntersectionOnImagePlane& obj);
    //!< Overloaded output-stream.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    void
    calculate_image_plane();
    //!< Determines the image plane based on focal length (\ref f) and \ref axis_toward_scene.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    float
    get_f() const;
    //!< Returns the corresponding data member.

    virtual
    int
    get_axis_toward_scene() const;
    //!< Returns the corresponding data member.

    virtual
    float
    get_row() const;
    //!< Returns the corresponding data member.

    virtual
    float
    get_col() const;
    //!< Returns the corresponding data member.

    virtual
    float
    get_d() const;
    //!< Returns the corresponding data member.

    virtual
    float
    get_z() const;
    //!< Returns the corresponding data member.

    virtual
    float
    get_z_scale() const;
    //!< Returns the corresponding data member.

    virtual
    uint8_t
    get_r() const;
    //!< Returns the corresponding data member.

    virtual
    uint8_t
    get_g() const;
    //!< Returns the corresponding data member.

    virtual
    uint8_t
    get_b() const;
    //!< Returns the corresponding data member.

    /*!
     * \brief Returns the color data in RGB format.
     * \sa get_r, get_g, get_b, get_bgr, get_color_RGB and get_color_BGR.
     * \return
     */
    virtual
    int
    get_rgb() const;

    /*!
     * \brief Returns the color data in BGR format.
     * \sa get_r, get_g, get_b, get_rgb, get_color_RGB and get_color_BGR.
     * \return
     */
    virtual
    int
    get_bgr() const;

    /*!
     * \brief Returns the color data in OpenCV RGB format.
     * \sa get_r, get_g, get_b, get_rgb, get_bgr and get_color_BGR.
     * \return
     */
    virtual
    cv::Vec3b
    get_color_RGB() const;

    /*!
     * \brief Returns the color data in OpenCV BGR format.
     * \sa get_r, get_g, get_b, get_rgb, get_bgr and get_color_RGB.
     * \return
     */
    virtual
    cv::Vec3b
    get_color_BGR() const;

    virtual
    hjh::Plane<float>
    get_image_plane() const;
    //!< Returns the corresponding data member (\ref Phi).
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    void
    set_z_scale( float scale_ );
    //!< Set the corresponding data member.

    /*!
     * \brief set_focal_length sets the the corresponding data member (it prevents
     * invalid updates).
     * \note it is required to update the image plane via \ref calculate_image_plane.
     */
    virtual
    void
    set_focal_length( float focal_length_ );

    /*!
     * \brief set_focal_length sets the the corresponding data member,\ref f,
     * (it prevents invalid updates).
     * \note it is required to update the image plane via \ref calculate_image_plane.
     */
    virtual
    void
    set_axis_toward_scene( int axis_to_scene_ );


    virtual
    void
    set_image_plane( const hjh::Plane<float> &ptr );
    //!< Sets the the corresponding data member.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    /*!
     * \brief intersect_with_ray inersects the image plane, \ref Phi, with a ray
     * originating from a source point.
     * \param[in]   source_point_   represents the origin of the ray.
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_, uint8_t r_, uint8_t g_, uint8_t b_ ).
     * \sa intersect_with_ray( pcl::PointXYZ source_point_ ).
     * \sa intersect_with_ray( pcl::PointXYZRGB source_point_ ).
     */
    virtual
    void
    intersect_with_ray( const hjh::Point3D<float> &source_point_ );

    /*!
     * \brief An alias for the \ref
     * intersect_with_ray( hjh::Point3D<float> source_point_ ) with color data.
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_ ).
     * \sa intersect_with_ray( pcl::PointXYZ source_point_ ).
     * \sa intersect_with_ray( pcl::PointXYZRGB source_point_ ).
     */
    virtual
    void
    intersect_with_ray( const hjh::Point3D<float> &source_point_
                        , uint8_t r_, uint8_t g_, uint8_t b_ );

    /*!
     * \brief An alias for the \ref
     * intersect_with_ray( hjh::Point3D<float> source_point_ ) compatible with
     * <a href="http://pointclouds.org/">PCL</a> format.
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_ ).
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_, uint8_t r_, uint8_t g_, uint8_t b_ ).
     * \sa intersect_with_ray( pcl::PointXYZRGB source_point_ ).
     */
    virtual
    void
    intersect_with_ray( const pcl::PointXYZ &source_point_PCL_ );

    /*!
     * \brief An alias for the \ref
     * intersect_with_ray( hjh::Point3D<float> source_point_ ) containing color
     * information and compatible with <a href="http://pointclouds.org/">PCL</a>
     * format.
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_ ).
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_, uint8_t r_, uint8_t g_, uint8_t b_ ).
     * \sa intersect_with_ray( pcl::PointXYZ source_point_ ).
     */
    virtual
    void
    intersect_with_ray( const pcl::PointXYZRGB &source_point_PCL_ );

    /*!
     * \brief An alias for the \ref
     * intersect_with_ray( hjh::Point3D<float> source_point_ ) containing color
     * information and compatible with <a href="http://pointclouds.org/">PCL</a>
     * format.
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_ ).
     * \sa intersect_with_ray( hjh::Point3D<float> source_point_, uint8_t r_, uint8_t g_, uint8_t b_ ).
     * \sa intersect_with_ray( pcl::PointXYZ source_point_ ).
     */
    virtual
    void
    intersect_with_ray( const pcl::PointXYZRGBA &source_point_PCL_ );
    //--------------------------------------------------------------------------


protected:

    /*!
     * \brief f represents the focal lenght of the corresponding camera (the
     * maximum of fx and fy).
     */
    float f;

    /*!
     * \brief axis_toward_scene indicates which axis is located toward the scene
     * (0=X-axis, 1=Y-axis, and 2=Z-axis).
     */
    int axis_toward_scene;

    float row;    //!< row represents the intersection point in terms on rows.
    float col;    //!< col represents the intersection point in terms on columns.

    /*!
     * \brief d indicates the distance between source point (origin of ray) to the <B>camera center</B>.
     */
    float d;

    /*!
     * \brief z represents the Z-buffer value of the source point, which is
     * defined as the distance of source point to the <B>image plane</B>.
     */
    float z;

    /*!
     * \brief z_scale defines the scale used for calculating the \ref z value.
     * For instance, by default it is assumed as 1000 to scale meter to millimeter.
     */
    float z_scale;

    uint8_t r;  //!< the red channel of color value
    uint8_t g;  //!< the green channel of color value
    uint8_t b;  //!< the blue channel of color value

    /*!
     * \brief Phi defines the image plane based on focal length (\ref f) with
     * unity size an each half (a 1+1 by 1+1 image).
     * \note This image is presented as continuous data instead of descrete pixels.
     */
    hjh::Plane<float> Phi; // image plane


protected:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler \note Phi is excluded.
};

//BOOST_CLASS_VERSION( RayIntersectionOnImagePlane, 1 );

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/RayIntersectionOnImagePlane_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
