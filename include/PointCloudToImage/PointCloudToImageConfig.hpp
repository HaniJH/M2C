#ifndef _POINT_CLOUD_TO_IMAGE_CONFIG_20150806_HPP_
#define _POINT_CLOUD_TO_IMAGE_CONFIG_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/FuseColorData.hpp"
#include "PointCloudToImage/FuseDepthData.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************     E N U E R A T I O N S   ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************         C L A S S E S       ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

/*!
 * \brief The PointCloudToImageConfig class contains the required setting values
 * for extracting images out of a pointcloud.
 */
class PointCloudToImageConfig
{
public:

    PointCloudToImageConfig();
    //!< Default constructor.

    PointCloudToImageConfig( double horizontal_fov_, double vertical_fov_
                             , double voxel_size_, int axis_
                             , unsigned int desired_rows, unsigned int desired_cols );
    //!< Constructor.

    virtual
    ~PointCloudToImageConfig();
    //!< Destructor.


    typedef boost::shared_ptr<PointCloudToImageConfig> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const PointCloudToImageConfig> ConstPtr;
    //!< Const shared pointer.


    friend std::ostream&
    operator<<(std::ostream& os, const PointCloudToImageConfig& obj);
    //!< Overloaded output-stream.


private:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler.

public:

    double horizontal_FOV;
    //!< Horizontal field of view in degrees.

    double vertical_FOV;
    //!< Vetical field of view in degrees.

    double voxel_size;
    //!< Resolution of voxel-grid in meters (and resolutin of iamge accordingly).

    int axis_toward_scene;
    //!< Determines the axis which is pointing at the scene. (0:X, 1:Y, 2:Z)

    unsigned int desired_image_size_rows;
    //!< Indicates the desired number of rows in the produced image.

    unsigned int desired_image_size_cols;
    //!< Indicates the desired number of columns in the produced image.

    FuseColorData fuse_color_data;
    //!< only used for model-to-camera back raycasting.

    FuseDepthData fuse_depth_data;
    //!< only used for model-to-camera back raycasting.
};

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/PointCloudToImageConfig_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
