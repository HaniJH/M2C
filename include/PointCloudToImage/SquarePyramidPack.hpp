#ifndef _SQUARE_PYRAMID_PACK_20150806_HPP_
#define _SQUARE_PYRAMID_PACK_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "haniDefinitions.hpp"
#include "PointCloudToImage/PointCloudToPyramidLog.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

//==============================================================================
//==============================================================================

/*!
 * \brief The SquarePyramidPack class encapsulates the 2D and 3D information regarding
 * any square pyramid culled from a point cloud.
 */
class SquarePyramidPack
{
public:

    SquarePyramidPack();
    //! Default constructor.

    /*!
     * \brief Constructor.
     * \note filename_ should contain only the file name (stem) <i>without any extension</i>.
     */
    SquarePyramidPack( std::string filename_, double hfov_
                          , double vfov_, hjh::Pose pose_ );


    cv::Mat color_image;
    //!< The visual (RGB) data of the square pyramid (CV_8UC3).

    cv::Mat depth_image;
    //!< The depth data of the square pyramid (CV_16UC1) in millimeter.

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr voxelized_cloud_ptr;
    //!< The voxelized FOV-filtered cloud of the square pyramid as a smart pointer.

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr original_cloud_ptr;
    //!< The original FOV-filtered cloud of the square pyramid as a smart pointer.

    PointCloudToPyramidLog log;
    //!< Contains the log information.
};

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
