#ifndef POINT_CLOUD_PARSING_CONFIG_20150806_HPP_
#define POINT_CLOUD_PARSING_CONFIG_20150806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/PointCloudToImageConfig.hpp"
#include "haniUtilities.hpp"

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

namespace hjh {
namespace pcd2img {

//==============================================================================
//==============================================================================


/*!
 * \brief The PointCloudParsingConfig class includes all the settings needed for
 * parsing a large point cloud to generate corresponding images.
 *
 * \todo set and get methods to have a control over data member assignments
 * e.g. h_overlap should be in [0,1) etc.
 */
class PointCloudParsingConfig{

public:

    PointCloudParsingConfig();
    //!< Default constructor.

    PointCloudParsingConfig( PointCloudToImageConfig cfg_
                             , double h_overlap_, double v_overlap
                             , double max_hfov_, double min_hfov_
                             , double max_vfov_, double min_vfov_
                             );
    //!< Constructor (initializes the corresponding data members).

    virtual
    ~PointCloudParsingConfig();
    //!< Destructor.


    typedef boost::shared_ptr<PointCloudParsingConfig> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const PointCloudParsingConfig> ConstPtr;
    //!< Const shared pointer.


    friend std::ostream&
    operator<<(std::ostream& os, const PointCloudParsingConfig& obj);
    //!< Overloaded output-stream.


    /*!
     * \brief get_horizontal_steps calculates each step (round of raycasting)
     * as its initial horizontal FOV in degree.
     * \return The horizontal steps as a vector.
     * \note The size of resulting vector idicates the number of steps.
     */
    virtual
    std::vector<double>
    get_horizontal_steps() const;

    /*!
     * \brief get_horizontal_steps calculates each step (round of raycasting)
     * as its initial vertical FOV in degree.
     * \return The horizontal steps as a vector.
     * \note The size of resulting vector idicates the number of steps.
     */
    virtual
    std::vector<double>
    get_vertical_steps() const;


private:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler.

public:

    PointCloudToImageConfig pcd_to_img_cfg;
    //!< Contains the configuration for each round of producing an image.

    double horizontal_overlap;
    //!< The horizontal overlap between a pair of censequtive images in percentage; accepted range: [0,1).

    double vertical_overlap;
    //!< The vertical overlap between a pair of censequtive images in percentage; accepted range: [0,1).

    double max_horizontal_FOV;
    //!< The maximum horizontal field of view in degree; accepted range: (0,360].

    double min_horizontal_FOV;
    //!< The minimum horizontal field of view in degree; accepted range: [0,360).

    double max_vertical_FOV;
    //!< The maximum vertical field of view in degree; accepted range: (-90,90].

    double min_vertical_FOV;
    //!< The minimum vertical field of view in degree; accepted range: [-90,90).

    hjh::Pose virtual_camera_pose;
    //!< Indicates the pose for the virtual camera inside the ponit cloud.

};

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/PointCloudParsingConfig_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
