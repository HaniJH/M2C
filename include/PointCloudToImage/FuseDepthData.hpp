#ifndef _FUSE_DEPTH_DATA_20152806_HPP_
#define _FUSE_DEPTH_DATA_20152806_HPP_

// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include <boost/shared_ptr.hpp>

#include <opencv2/opencv.hpp>

#include "boost_serialization.hpp"

namespace hjh {
namespace pcd2img {

//==============================================================================
//==============================================================================

/*!
 * \brief The FuseDepthData class encapsulates the fusion process for depth data
 * in OpenCV format (CV_16UC2) to enable user updating any pixel in a depth image
 * with effecting its neighbours and based on a criterion factor.
 * \sa FuseColorData.
 */
class FuseDepthData
{
public:

    //--------------------------------------------------------------------------
    FuseDepthData();
    //! Default constructor.

    /*!
     * \brief FuseDepthData is one of the constructors.
     * \param[in]   target_image_   the target iamge.
     * \note it assumes that the target_image_ is in CV_16UC2 format.
     */
    FuseDepthData( const cv::Mat &target_image_ );

    /*!
     * \brief FuseDepthData is one of the constructors.
     * \param[in]   rows_   the row size of the target iamge.
     * \param[in]   cols_   the column size of the target iamge.
     */
    FuseDepthData( int rows_, int cols_ );

    virtual
    ~FuseDepthData();
    //! Destructor.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    typedef boost::shared_ptr<FuseDepthData> Ptr;
    //!< Shared pointer.

    typedef boost::shared_ptr<const FuseDepthData> ConstPtr;
    //!< Const shared pointer.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    friend std::ostream&
    operator<<(std::ostream& os, const FuseDepthData& obj);
    //!< Overloaded output-stream.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    /*!
     * \brief fuse_new_pixel integrates a new pixel into the target image by
     * effecting all its neighbours.
     * \param[in]   row_    row of the target pixel.
     * \param[in]   col_    column of the target pixel.
     * \param[in]   depth_  the new pixel depth value.
     * \param[in]   factor_ the criterion factor to decide about integration.
     * \return 'True' if it does the job successfully, and 'False' otherwise.
     */
    virtual
    bool
    fuse_new_pixel( int row_, int col_, uint16_t depth_, float factor_ );
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    virtual
    cv::Mat
    get_target_image() const;
    //!< Returns the the corresponding data member.

    virtual
    cv::Mat
    get_total_weight() const;
    //!< Returns the the corresponding data member.

    virtual
    cv::Mat
    get_last_updated_factor() const;
    //!< Returns the the corresponding data member.

    virtual
    bool
    get_flag_criterion_factor() const;
    //!< Returns the the corresponding data member.

    virtual
    float
    get_tolerance() const;
    //!< Returns the the corresponding data member.

    virtual
    int
    get_plus_neighbours_weight() const;
    //!< Returns the the corresponding data member.

    virtual
    int
    get_cross_neighbours_weight() const;
    //!< Returns the the corresponding data member.

    virtual
    int
    get_direct_pixel_weight() const;
    //!< Returns the the corresponding data member.

    virtual
    bool
    get_flag_affect_neighbours() const;
    //!< Returns the the corresponding data member.

    virtual
    bool
    get_flag_only_empty_neighbours() const;
    //!< Returns the the corresponding data member.
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    /*!
     * \brief Sets the the corresponding data member (target_image) and also
     * creates the total_weight and last_updated_factor elements accrodingly.
     */
    virtual
    void
    set_target_image( const cv::Mat &image_ );

    virtual
    void
    set_flag_criterion_factor( bool flag_ );
    //!< Sets the the corresponding data member.

    virtual
    void
    set_tolerance( float value_ );
    //!< Sets the the corresponding data member.

    virtual
    void
    set_plus_neighbours_weight( int weight_ );
    //!< Sets the the corresponding data member.

    virtual
    void
    set_cross_neighbours_weight( int weight_ );
    //!< Sets the the corresponding data member.

    virtual
    void
    set_direct_pixel_weight( int weight_ );
    //!< Sets the the corresponding data member.

    virtual
    void
    set_flag_affect_neighbours( bool flag_ );
    //!< Sets the the corresponding data member.

    virtual
    void
    set_flag_only_empty_neighbours( bool flag_ );
    //!< Sets the the corresponding data member.
    //--------------------------------------------------------------------------


//------------------------------------------------------------------------------
//  h e l p e r   m e t h o d s
//------------------------------------------------------------------------------

private:

    //--------------------------------------------------------------------------
    /*!
     * This enum indicates each of the four corners of an image.
     */
    enum WHICH_CORNER
        {
        NO_CORNER = 0               //!< None of the corners.
        , TOP_RIGHT_CORNER = 1      //!< top-right corner.
        , TOP_LEFT_CORNER = 2       //!< top-left corner.
        , BOTTOM_RIGHT_CORNER = 3   //!< bottom-right corner.
        , BOTTOM_LEFT_CORNER = 4    //!< bottom-left corner.
         };

    /*!
     * \brief is_pixel_on_border checks if pixel(row_, col_) is located on one
     * of the corners of image or not (helper method).
     * \note the result can be processed also as an boolean data.
     * \return the result based on \ref WHICH_CORNER.
     */
    inline
    WHICH_CORNER
    is_pixel_on_corner( int row_, int col_ ) const;
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    /*!
     * This enum indicates each of the four borders of an image (excluding the
     * four corner points).
     */
    enum WHICH_BORDER
        {
        NO_BORDER = 0       //!< None of the borders.
        , TOP_BORDER = 1    //!< top border.
        , RIGHT_BORDER = 2  //!< right border.
        , BOTTOM_BORDER = 3 //!< bottom border.
        , LEFT_BORDER = 4   //!< left border.
         };

    /*!
     * \brief is_pixel_on_border checks if pixel(row_, col_) is located on the
     * frame (wich part) or not (helper method).
     * \note frame is the 1-pixel wide strips on borders of an image.
     * \note the result can be processed also as an boolean data.
     * \note the corners are not detected as border points.
     * \return the result according to \ref WHICH_BORDER.
     */
    inline
    WHICH_BORDER
    is_pixel_on_border( int row_, int col_ ) const;
    //--------------------------------------------------------------------------

     /*!
     * \brief is_first_update indicates if pixel(row_, col_) have already been
     * update or not.
     * \return 'True' if it has not been updated yet, 'False' otherwise.
     */
    inline
    bool
    is_first_update( int row_, int col_ ) const;

    /*!
     * This enum indicates three different situations happens for a new pixel
     * based on its factor value.
     */
    enum NEW_PIXEL_STATUS
        {
            UNKNOWN = 0
            //!< an invalid status.
            , DISCARD = 1
            //!< Discard the new pixel.
            , INTEGRATE = 2
            //!< Integrate the new pixel into the existing one.
            , REPLACE = 3
            //!< Replace the existing one with the new pixel.
         };

    /*!
     * \brief get_new_pixel_status returns the status of the pixel(row_, col_).
     */
    inline
    NEW_PIXEL_STATUS
    get_new_pixel_status( int row_, int col_, float new_factor_ );

    inline
    void
    update_last_updated_factor( int row_, int col_, float new_factor_ );
    //!< updates the last_updated_factor for pixel(row_, col_).

    /*!
     * \brief fuse_color integrates a single color into pixel(row_, col_) with
     * accumulated weight.
     * \param[in]   row_            row of the target pixel.
     * \param[in]   col_            column of the target pixel.
     * \param[in]   new_weight_     new weight.
     * \param[in]   new_depth_      new depth.
     * \param[in]   flag_only_empty if 'true', replaces the color only if the
     * target pixel is zero (empty).
     */
    inline
    void
    fuse_depth( int row_, int col_, int new_weight_, const uint16_t new_depth_
                , bool flag_only_empty = false );

    /*!
     * \brief replace_color replaces the color of pixel(row_, col_) with the
     * new_color_ and also update the weight.
     * \param[in]   row_            row of the target pixel.
     * \param[in]   col_            column of the target pixel.
     * \param[in]   new_weight_     new weight.
     * \param[in]   new_depth_      new depth.
     * \param[in]   flag_only_empty if 'true', replaces the color only if the
     * target pixel is zero (empty).
     */
    inline
    void
    replace_depth( int row_, int col_, int new_weight_, uint16_t new_depth_
                   , bool flag_only_empty = false );

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------


protected:

    /*!
     * \brief target_image contains the pixels of the depth data as the target
     * image (in OpenCV CV_16UC1 foramt).
     */
    cv::Mat target_image;

    /*!
     * \brief total_weight contains the corresponding weight for each pixel of
     * \ref target_image (in OpenCV CV_32SC1 integer foramt).
     */
    cv::Mat total_weight;

    /*!
     * \brief last_updated_factor indicates the criterion factor for each pixel
     * to decide about updating or not updating the target pixels (in OpenCV
     * CV_32FC1 float foramt).
     * \sa flag_criterion_factor.
     */
    cv::Mat last_updated_factor;

    /*!
     * \brief flag_criterion_factor is a bolean flag used for interpreting the
     *  \ref last_updated_factor values: flag ON means ascending order and flag
     * OFF means descending order for comparing last_updated_factor values.
     * \note flag ON keeps the smaller values and discard the larger ones, and
     * vice-versa for the flag OFF (by default ON).
     * e.g. this criterion could be the distance of a point to the camera center.
     */
    bool flag_criterion_factor;

    /*!
     * \brief tolrance used in comparison of \ref last_updated_factor values with
     * the new values.
     * \note There can be 3 different situations:
     *      -# new_value <= (1 - tolrance) * recent_value,
     *      -# (1 - tolrance) * recent_value < new_value <= (1 + tolrance) * recent_value,
     *      -# (1 + tolrance) * recent_value > new_value.
     */
    float tolerance;

    /*!
     * \brief plus_neighbours_w
     * eight is the share of the PLUS neighbors around
     * target point <B>T</B> as depicted by '+' below:
     * <table border='1'>
     * <tr><th></th>.<th>+</th><th>.</th></tr>
     * <tr><th>+</th><td><B>T</B></td><td>+</td></tr>
     * <tr><th>.</th><td>+</td><td>.</td></tr>
     * </table>
     */
    int plus_neighbours_weight;

    /*!
     * \brief plus_neighbours_w
     * eight is the share of the CROSS neighbors around
     * target point <B>T</B> as depicted by 'x' below:
     * <table border='1'>
     * <tr><th></th>x<th>.</th><th>x</th></tr>
     * <tr><th>.</th><td><B>T</B></td><td>.</td></tr>
     * <tr><th>x</th><td>.</td><td><B>x</B></td></tr>
     * </table>
     */
    int cross_neighbours_weight;

    /*!
     * \brief direct_pixel_weight indicates the corresponding weight for the
     * direct target pixel.
     */
    int direct_pixel_weight;

    /*!
     * \brief flag_affect_neighbours indicates wether to update the neighbours
     * of a pixel or not during updating the pixel itself.
     * \sa flag_only_empty_neighbours.
     */
    bool flag_affect_neighbours;

    /*!
     * \brief flag_only_empty_neighbours works along with the \ref flag_affect_neighbours
     * and limits the affected neighbours to only the empty (zero) ones.
     * \sa flag_affect_neighbours.
     */
    bool flag_only_empty_neighbours;

protected:

    friend class boost::serialization::access;
    //!< To handle boost serialization.

    template<class Archive>
    void
    serialize( Archive & ar, const unsigned int version );
    //!< Boost serialization handler \note Phi is excluded.
};

//==============================================================================
//==============================================================================

// *****************************************************************************
// *****************************************************************************

} // end of namespace pcd2img
} // end of namespace hjh

#include "PointCloudToImage/FuseDepthData_imp.hpp"

#endif
// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
