
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/PointCloudToImageLog.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

PointCloudToImageLog::PointCloudToImageLog()
    : filename( "color_image.png" )
    , vertical_FOV( 0 )
    , pose( hjh::Pose() )
{
    pose.posename = "virtual camera";
}

//==============================================================================
//==============================================================================

PointCloudToImageLog::PointCloudToImageLog( std::string filename_, double hfov_
                                            , double vfov_, hjh::Pose pose_ )
    : filename( filename_ )
    , horizontal_FOV( hfov_ )
    , vertical_FOV( vfov_ )
    , pose( pose_ )
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudToImageLog::~PointCloudToImageLog()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<(std::ostream& os, const PointCloudToImageLog& obj)
{
//    os
//        << "     color image filename : " << obj.color_image_filename
//        << "\n     depth image filename : " << obj.depth_image_filename
//        << "\n  original cloud filename : " << obj.original_cloud_filename
//        << "\n voxelized cloud filename : " << obj.voxelized_cloud_filename
//        << "\n     horizontal FOV (deg) : " << obj.horizontal_FOV
//        << "\n       vertical FOV (deg) : " << obj.vertical_FOV
//        << "\n                pose-name : " << obj.pose.posename
//        << "\n            pose-rotation : " << obj.pose.rotation
//        << "\n         pose-translation : " << obj.pose.translation
//        ;
//    return os;
    os
        << "                 filename : " << obj.filename
        << "\n     horizontal FOV (deg) : " << obj.horizontal_FOV
        << "\n       vertical FOV (deg) : " << obj.vertical_FOV
        << "\n                pose-name : " << obj.pose.posename
        << "\n            pose-rotation : " << obj.pose.rotation
        << "\n         pose-translation : " << obj.pose.translation
        ;
    return os;
}

//==============================================================================
//==============================================================================


} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
