
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/PointCloudToPyramidLog.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

PointCloudToPyramidLog::PointCloudToPyramidLog()
    : PointCloudToImageLog()
    , color_image_filename( "color_image.png" )
    , depth_image_filename( "depth_image.png" )
    , original_cloud_filename( "original_cloud.pcd" )
    , voxelized_cloud_filename( "voxelized_cloud.pcd" )
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudToPyramidLog::PointCloudToPyramidLog( std::string filename_, double hfov_
                                            , double vfov_, hjh::Pose pose_ )
    : PointCloudToImageLog( filename_ , hfov_, vfov_, pose_ )
    , color_image_filename( "color_image_" + filename_ + ".png" )
    , depth_image_filename( "depth_image_" + filename_ + ".png" )
    , original_cloud_filename(   "original_cloud_" + filename_ + ".pcd" )
    , voxelized_cloud_filename( "voxelized_cloud_" + filename_ + ".pcd" )
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudToPyramidLog::~PointCloudToPyramidLog()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<(std::ostream& os, const PointCloudToPyramidLog& obj)
{
    os
        << "     color image filename : " <<   obj.color_image_filename
        << "\n     depth image filename : " << obj.depth_image_filename
        << "\n  original cloud filename : " << obj.original_cloud_filename
        << "\n voxelized cloud filename : " << obj.voxelized_cloud_filename
        << "\n     horizontal FOV (deg) : " << obj.horizontal_FOV
        << "\n       vertical FOV (deg) : " << obj.vertical_FOV
        << "\n                pose-name : " << obj.pose.posename
        << "\n            pose-rotation : " << obj.pose.rotation
        << "\n         pose-translation : " << obj.pose.translation
        ;
    return os;
}

//==============================================================================
//==============================================================================

} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
