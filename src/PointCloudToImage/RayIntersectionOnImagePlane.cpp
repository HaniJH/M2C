
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/RayIntersectionOnImagePlane.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

RayIntersectionOnImagePlane::RayIntersectionOnImagePlane()
    : f( 1.0f )
    , axis_toward_scene( 0 )
    , row( 0 )
    , col( 0 )
    , d( 0 )
    , z( 0 )
    , z_scale( 1000.0f )
    , r( 140 )
    , g( 140 )
    , b( 140 )
{
    // update the image plane accrodingly
    calculate_image_plane();
}

//==============================================================================
//==============================================================================

RayIntersectionOnImagePlane::RayIntersectionOnImagePlane(
        float focal_length_ , int axis_to_scene_ )
    : f( 1.0f ) // temporary initialization
    , axis_toward_scene( 0 ) // temporary initialization
    , row( 0 )
    , col( 0 )
    , d( 0 )
    , z( 0 )
    , z_scale( 1000.0f )
    , r( 140 )
    , g( 140 )
    , b( 140 )
{
    // set focal length
    set_focal_length( focal_length_ );

    // set axis
    set_axis_toward_scene( axis_to_scene_ );

    // update the image plane accrodingly
    calculate_image_plane();
}

//==============================================================================
//==============================================================================

RayIntersectionOnImagePlane::~RayIntersectionOnImagePlane()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<(std::ostream& os, const RayIntersectionOnImagePlane& obj)
{
    os
            << "  f   (focal length) : " << obj.get_f()
            << "\n  row       (height) : " << obj.get_row()
            << "\n  col        (width) : " << obj.get_col()
            << "\n  d       (distance) : " << obj.get_d()
            << "\n  z       (Z-buffer) : " << obj.get_z()
            << "\n  z_scale (Z-buffer) : " << obj.get_z()
            << "\n  r      (Red color) : " << (int)obj.get_r()
            << "\n  g    (Green color) : " << (int)obj.get_g()
            << "\n  b     (Blue color) : " << (int)obj.get_b()
            << "\nPhi    (image plane) : " << obj.get_image_plane()
    ;
    return os;
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::calculate_image_plane()
{
    switch( axis_toward_scene )
    {
    case 0: // X-axis
        Phi.set_a( -1 );
        Phi.set_b( 0 );
        Phi.set_c( 0 );
        Phi.set_d( f );
        break;

    case 1: // Y-axis
        Phi.set_a( 0 );
        Phi.set_b( -1 );
        Phi.set_c( 0 );
        Phi.set_d( f );
        break;

    case 2: // Z-axis
        Phi.set_a( 0 );
        Phi.set_b( 0 );
        Phi.set_c( -1 );
        Phi.set_d( f );
        break;

    default:
        // BADAN ToDo throw an exception
        break;
    }
}

//==============================================================================
//==============================================================================

float
RayIntersectionOnImagePlane::get_f() const
{
    return f;
}

//==============================================================================
//==============================================================================

int
RayIntersectionOnImagePlane::get_axis_toward_scene() const
{
    return axis_toward_scene;
}

//==============================================================================
//==============================================================================

float
RayIntersectionOnImagePlane::get_row() const
{
    return row;
}

//==============================================================================
//==============================================================================

float
RayIntersectionOnImagePlane::get_col() const
{
    return col;
}

//==============================================================================
//==============================================================================

float
RayIntersectionOnImagePlane::get_d() const
{
    return d;
}

//==============================================================================
//==============================================================================

float
RayIntersectionOnImagePlane::get_z() const
{
    return z;
}

//==============================================================================
//==============================================================================

float
RayIntersectionOnImagePlane::get_z_scale() const
{
    return z_scale;
}

//==============================================================================
//==============================================================================

uint8_t
RayIntersectionOnImagePlane::get_r() const
{
    return r;
}

//==============================================================================
//==============================================================================

uint8_t
RayIntersectionOnImagePlane::get_g() const
{
    return g;
}

//==============================================================================
//==============================================================================

uint8_t
RayIntersectionOnImagePlane::get_b() const
{
    return b;
}

//==============================================================================
//==============================================================================

int
RayIntersectionOnImagePlane::get_rgb() const
{
    return int((r << 16) + (g << 8) + b);
}

//==============================================================================
//==============================================================================

int
RayIntersectionOnImagePlane::get_bgr() const
{
    return int((b << 16) + (g << 8) + r);
}

//==============================================================================
//==============================================================================

cv::Vec3b
RayIntersectionOnImagePlane::get_color_RGB() const
{
    cv::Vec3b color;
    color[0] = r;
    color[1] = g;
    color[2] = b;
    return color;
}

//==============================================================================
//==============================================================================

cv::Vec3b
RayIntersectionOnImagePlane::get_color_BGR() const
{
    cv::Vec3b color;
    color[0] = b;
    color[1] = g;
    color[2] = r;
    return color;
}

//==============================================================================
//==============================================================================

hjh::Plane<float>
RayIntersectionOnImagePlane::get_image_plane() const
{
    return Phi;
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::set_z_scale( float scale_ )
{
    z_scale = scale_;
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::set_focal_length( float focal_length_ )
{
    if ( focal_length_ > 0 )
    {
        f = focal_length_;
    }
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::set_axis_toward_scene( int axis_to_scene_ )
{
    if ( axis_to_scene_<0 || axis_to_scene_>2 )
    {
        // BADAN ToDo throw an exception
        return;
    }

    axis_toward_scene = axis_to_scene_;
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::set_image_plane( const hjh::Plane<float> &ptr )
{
    Phi = ptr;
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::intersect_with_ray( const hjh::Point3D<float> &source_point_ )
{
    // create line passing through the source point and the camera center
    hjh::Point3D<float> camera_center(0,0,0), intersection_point(0,0,0);
    hjh::Line<float> line;
    line.update_equation( camera_center, source_point_ );

    //..........................................................................
    // updating row, col
    //..........................................................................
    // intersect
    hjh::RESULT_LINE_AND_PLANE_INTERSECTION intersection_result =
        Phi.intersection( line, intersection_point );

    switch( intersection_result )
    {
    case hjh::LINE_AND_PLANE_INTERSECTED:
        switch( axis_toward_scene )
        {
        case 0: // X-axis
            col = -intersection_point.y;
            row = -intersection_point.z;
            break;

        case 1: // Y-axis
            col = intersection_point.x;
            row = -intersection_point.z;
            break;

        case 2: // Z-axis
            col = intersection_point.x;
            row = intersection_point.y;
            break;
        }
        break;

    case hjh::NO_INTERSECTION_PARALLEL:
    case hjh::LINE_IS_CONTAINED_IN_PLANE:
    case hjh::INVALID_DATA_FOR_INTERSECTION:
    default:
        // throw an exception
        return;
    }

    //..........................................................................
    // updating d
    //..........................................................................

    d = camera_center.distance_f( source_point_ );

    //..........................................................................
    // updating z (Z-buffer)
    //..........................................................................

    // degree between ray and the center line prependicular to image plane
    float alpha_col = std::atan( std::abs(col) / f );
    float alpha_row = std::atan( std::abs(row) / f );

    // distance to the plane parallel to the image plane passing through camera center
    float z_ = d * std::cos( alpha_col ) * std::cos( alpha_row );

    // decrement the focal length out
    //z = (z_ - f) * z_scale;
    z = (z_ * z_scale) - f;

    // added by HJH @20160823 to overwrite the above statement
    z = z_ * z_scale;

    //..........................................................................
    // updating color
    //..........................................................................

    // empty
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::intersect_with_ray(
        const hjh::Point3D<float> &source_point_
        , uint8_t r_, uint8_t g_, uint8_t b_ )
{
    // updating color
    r = r_;
    g = g_;
    b = b_;

    // intersection...
    intersect_with_ray( source_point_ );
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::intersect_with_ray(
        const pcl::PointXYZ &source_point_PCL_ )
{
    // casting
    hjh::Point3D<float> source_point_( source_point_PCL_.x
                                       , source_point_PCL_.y
                                       , source_point_PCL_.z );

    // intersection...
    intersect_with_ray( source_point_ );
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::intersect_with_ray(
        const pcl::PointXYZRGB &source_point_PCL_ )
{
    // casting
    hjh::Point3D<float> source_point_( source_point_PCL_.x
                                       , source_point_PCL_.y
                                       , source_point_PCL_.z );

    // intersection...
    intersect_with_ray( source_point_
                        , source_point_PCL_.r
                        , source_point_PCL_.g
                        , source_point_PCL_.b );
}

//==============================================================================
//==============================================================================

void
RayIntersectionOnImagePlane::intersect_with_ray(
        const pcl::PointXYZRGBA &source_point_PCL_ )
{
    // casting
    hjh::Point3D<float> source_point_( source_point_PCL_.x
                                       , source_point_PCL_.y
                                       , source_point_PCL_.z );

    // intersection...
    intersect_with_ray( source_point_
                        , source_point_PCL_.r
                        , source_point_PCL_.g
                        , source_point_PCL_.b );
}

//==============================================================================
//==============================================================================


} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************

