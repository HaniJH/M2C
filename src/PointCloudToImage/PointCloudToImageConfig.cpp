
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/PointCloudToImageConfig.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

PointCloudToImageConfig::PointCloudToImageConfig()
    : horizontal_FOV( 70 )  // degree
    , vertical_FOV( 45 )    // degree
    , voxel_size( 0.01 )    // meter
    , axis_toward_scene( 2 )// Z-axis
    , desired_image_size_rows( 480 )
    , desired_image_size_cols( 640 )
    , fuse_color_data( 1, 1 )
    , fuse_depth_data( 1, 1 )
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudToImageConfig::PointCloudToImageConfig( double horizontal_fov_
                                                  , double vertical_fov_
                                                  , double voxel_size_
                                                  , int axis_
                                                  , unsigned int desired_rows
                                                  , unsigned int desired_cols )
    : horizontal_FOV( horizontal_fov_ )
    , vertical_FOV( vertical_fov_ )
    , voxel_size( voxel_size_ )
    , axis_toward_scene( axis_ )
    , desired_image_size_rows( desired_rows )
    , desired_image_size_cols( desired_cols )
    , fuse_color_data( 1, 1 )
    , fuse_depth_data( 1, 1 )
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudToImageConfig::~PointCloudToImageConfig()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<(std::ostream& os, const PointCloudToImageConfig& obj)
{
    os << " horizontal FOV (deg) : " << obj.horizontal_FOV
       << "\n   vertical FOV (deg) : " << obj.vertical_FOV
       << "\n       voxel size (m) : " << obj.voxel_size;
    switch ( obj.axis_toward_scene )
    {
    case 0: os << "\n    axix toward scene : X-axis (0)"; break;
    case 1: os << "\n    axix toward scene : Y-axis (1)"; break;
    case 2: os << "\n    axix toward scene : Z-axis (2)"; break;
    default: os << "\n    axix toward scene : invalid! ("
                << obj.axis_toward_scene << ")"; break;
    }
    os << "\n         desired rows : " << obj.desired_image_size_rows
       << "\n         desired cols : " << obj.desired_image_size_cols
       << "\n      fuse color data\n" << obj.fuse_color_data
       << "\n      fuse depth data\n" << obj.fuse_depth_data
          ;
    return os;
}

//==============================================================================
//==============================================================================


} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
