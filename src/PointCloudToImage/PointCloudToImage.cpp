
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/PointCloudToImage.hpp"

#include "haniUtilities.hpp"

#include <pcl/filters/voxel_grid.h>

#include <omp.h>


/*
//------------------------------------------------------------------------------
// Field of view and focal length
//------------------------------------------------------------------------------
// http://paulbourke.net/miscellaneous/lens/

// horizontal field of view = 2 atan(0.5 width / focallength)
// vertical field of view = 2 atan(0.5 height / focallength)
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Changing to/from vertical/horizontal aperture
//------------------------------------------------------------------------------
// http://paulbourke.net/miscellaneous/aperture/

// haperture = 2 atan[ width tan(vaperture/2) / height]
// vaperture = 2 atan[ height tan(haperture/2) / width]
//------------------------------------------------------------------------------
*/

namespace hjh {
namespace pcd2img {


// *****************************************************************************
// *********************     S T R U C T U R E S     ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************     P R O T O T Y P E S     ***************************
// *****************************************************************************

int
convert_pointcloud_to_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &image
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size );

int
convert_pointcloud_to_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &image
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size
        , const FuseColorData &fd );


int
convert_pointcloud_to_depth_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
        pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size );

int
convert_pointcloud_to_depth_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size );

int
convert_pointcloud_to_depth_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size
        , const FuseDepthData &fd );

int
convert_pointcloud_to_depth_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size
        , const FuseDepthData &fd );



inline
void
calculate_raycasted_image_resolution( double h_fov_deg      // in
                                      , double v_fov_deg    // in
                                      , double voxel_size   // in
                                      , int &h_res          // out
                                      , int &v_res );       // out



int
convert_pointcloud_to_square_pyramid_RAYCASTING_FROM_CAMERA_TO_MODEL(
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , hjh::pcd2img::SquarePyramidPack &pack
        , hjh::pcd2img::PointCloudToImageConfig cfg);

int
convert_pointcloud_to_square_pyramid_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , hjh::pcd2img::SquarePyramidPack &pack
        , hjh::pcd2img::PointCloudToImageConfig cfg);

// *****************************************************************************
// *********************        S T A T I C S        ***************************
// *****************************************************************************

// empty

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

const cv::Vec3b RED(0,0,200);
const cv::Vec3b BLUE(200,0,0);
const cv::Vec3b GREEN(0,200,0);
const cv::Vec3b ZERO_COLOR(0,0,0);

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &image
                             , double horizontal_fov
                             , double vertical_fov
                             , double voxel_size
                             , int axis_to_scene
                             , unsigned int desired_rows
                             , unsigned int desired_cols
                             , MODE mode
                             , const FuseColorData &fd )
{
    // check parameters

    // point cloud
    if (!cloud_ptr)
    {
        // BADAN throw an exception
        return -1;
    }

    // check fov
    if ( horizontal_fov <= 0 || vertical_fov <= 0 )
    {
        // BADAN throw an exception
        return -2;
    }

    // check the voxel size
    if ( voxel_size <= 0 )
    {
        // BADAN throw an exception
        return -3;
    }


    switch ( mode ) {
    case RAYCASTING_FROM_CAMERA_TO_MODEL:
        return convert_pointcloud_to_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
                    cloud_ptr, image, horizontal_fov, vertical_fov
                    , axis_to_scene, desired_rows, desired_cols, voxel_size );
        break;

    case BACK_RAYCASTING_FROM_MODEL_TO_CAMERA:
        return convert_pointcloud_to_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
                    cloud_ptr, image, horizontal_fov, vertical_fov
                    , axis_to_scene, desired_rows, desired_cols, voxel_size, fd );
        break;

    default:
        // BADAN throw an exception
        return -4;
        break;
    }

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
                                   , cv::Mat &dimage
                                   , double horizontal_fov
                                   , double vertical_fov
                                   , double voxel_size
                                   , int axis_to_scene
                                   , unsigned int desired_rows
                                   , unsigned int desired_cols
                                   , MODE mode
                                   , const FuseDepthData &fd )
{
    // check parameters

    // point cloud
    if (!cloud_ptr)
    {
        // BADAN throw an exception
        return -1;
    }

    // check fov
    if ( horizontal_fov <= 0 || vertical_fov <= 0 )
    {
        // BADAN throw an exception
        return -2;
    }

    // check the voxel size
    if ( voxel_size <= 0 )
    {
        // BADAN throw an exception
        return -3;
    }


    switch ( mode ) {
    case RAYCASTING_FROM_CAMERA_TO_MODEL:
        return convert_pointcloud_to_depth_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
                    cloud_ptr, dimage, horizontal_fov, vertical_fov
                    , axis_to_scene, desired_rows, desired_cols, voxel_size );
        break;

    case BACK_RAYCASTING_FROM_MODEL_TO_CAMERA:
        return convert_pointcloud_to_depth_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
                    cloud_ptr, dimage, horizontal_fov, vertical_fov
                    , axis_to_scene, desired_rows, desired_cols, voxel_size, fd );
        break;

    default:
        // BADAN throw an exception
        return -4;
        break;
    }

    return 0;
}


int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                                   , cv::Mat &dimage
                                   , double horizontal_fov
                                   , double vertical_fov
                                   , double voxel_size
                                   , int axis_to_scene
                                   , unsigned int desired_rows
                                   , unsigned int desired_cols
                                   , MODE mode
                                   , const FuseDepthData &fd )
{
    // check parameters

    // point cloud
    if (!cloud_ptr)
    {
        // BADAN throw an exception
        return -1;
    }

    // check fov
    if ( horizontal_fov <= 0 || vertical_fov <= 0 )
    {
        // BADAN throw an exception
        return -2;
    }

    // check the voxel size
    if ( voxel_size <= 0 )
    {
        // BADAN throw an exception
        return -3;
    }


    switch ( mode ) {
    case RAYCASTING_FROM_CAMERA_TO_MODEL:
        return convert_pointcloud_to_depth_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
                    cloud_ptr, dimage, horizontal_fov, vertical_fov
                    , axis_to_scene, desired_rows, desired_cols, voxel_size );
        break;

    case BACK_RAYCASTING_FROM_MODEL_TO_CAMERA:
        return convert_pointcloud_to_depth_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
                    cloud_ptr, dimage, horizontal_fov, vertical_fov
                    , axis_to_scene, desired_rows, desired_cols, voxel_size, fd );
        break;

    default:
        // BADAN throw an exception
        return -4;
        break;
    }

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &image
                             , hjh::pcd2img::PointCloudToImageConfig cfg
                             , MODE mode )
{
    return convert_pointcloud_to_image( cloud_ptr, image
                                        , cfg.horizontal_FOV
                                        , cfg.vertical_FOV
                                        , cfg.voxel_size
                                        , cfg.axis_toward_scene
                                        , cfg.desired_image_size_rows
                                        , cfg.desired_image_size_cols
                                        , mode
                                        , cfg.fuse_color_data );
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
                             , cv::Mat &dimage
                             , hjh::pcd2img::PointCloudToImageConfig cfg
                             , MODE mode )
{
    return convert_pointcloud_to_depth_image( cloud_ptr, dimage
                                        , cfg.horizontal_FOV
                                        , cfg.vertical_FOV
                                        , cfg.voxel_size
                                        , cfg.axis_toward_scene
                                        , cfg.desired_image_size_rows
                                        , cfg.desired_image_size_cols
                                        , mode
                                        , cfg.fuse_depth_data );
}

int
convert_pointcloud_to_depth_image( pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
                             , cv::Mat &dimage
                             , hjh::pcd2img::PointCloudToImageConfig cfg
                             , MODE mode )
{
    return convert_pointcloud_to_depth_image( cloud_ptr, dimage
                                        , cfg.horizontal_FOV
                                        , cfg.vertical_FOV
                                        , cfg.voxel_size
                                        , cfg.axis_toward_scene
                                        , cfg.desired_image_size_rows
                                        , cfg.desired_image_size_cols
                                        , mode
                                        , cfg.fuse_depth_data );
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &image
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size )
{
    //--------------------------------------------------------------------------
    // 1 - resulting-image resolution
    // 2 - create output image (RGB image)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == desired_rows || 0 == desired_cols )
    {
        calculate_raycasted_image_resolution( horizontal_fov, vertical_fov, voxel_size
                                              ,horizontal_resolution, vertical_resolution );
    }

    if ( 0 != desired_rows ) vertical_resolution = desired_rows;
    if ( 0 != desired_cols ) horizontal_resolution = desired_cols;

    image = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_8UC3 );

    //--------------------------------------------------------------------------
    // 3 - calculate delta fov's
    //--------------------------------------------------------------------------

    // in degree
    double horizontal_delta_fov = horizontal_fov / horizontal_resolution;
    double vertical_delta_fov = vertical_fov / vertical_resolution;

    //--------------------------------------------------------------------------
    // 4 - voxelizing the 3D model
    //--------------------------------------------------------------------------

    boost::shared_ptr< pcl::VoxelGrid<pcl::PointXYZRGB> > vg_ptr( new pcl::VoxelGrid<pcl::PointXYZRGB>() );

    // set the flag ON for fast access
    vg_ptr->setSaveLeafLayout( true );

    vg_ptr->setInputCloud( cloud_ptr );
    vg_ptr->setLeafSize( voxel_size, voxel_size, voxel_size );
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr voxelized_model( new pcl::PointCloud<pcl::PointXYZRGB>() );
    vg_ptr->filter( *voxelized_model );

    // swap the source cloud with the voxelizing result
    voxelized_model.swap( cloud_ptr );

//    std::vector<int> indexes = vg_ptr->getLeafLayout();

//    std::cout << "points: " << cloud_ptr->size() << std::endl;
//    std::cout << "data: " << indexes.size() << std::endl;

    //--------------------------------------------------------------------------
    // * - coordinates of min and max indexes
    //--------------------------------------------------------------------------

    Eigen::Vector3i mins = vg_ptr->getMinBoxCoordinates();
    Eigen::Vector3i maxs = vg_ptr->getMaxBoxCoordinates();
    Eigen::Vector3f leaf_size = vg_ptr->getLeafSize();

    float min_x = mins[0] * leaf_size[0];
    float min_y = mins[1] * leaf_size[1];
    float min_z = mins[2] * leaf_size[2];

    float max_x = maxs[0] * leaf_size[0];
    float max_y = maxs[1] * leaf_size[1];
    float max_z = maxs[2] * leaf_size[2];


    //--------------------------------------------------------------------------
    //  5 - find the front and back planes:
    //      - find the 4 corner points for each plane
    //      - create planes based on any desired but consistent 3 points
    //--------------------------------------------------------------------------

    hjh::Point3D<float> near_left_bottom;
    hjh::Point3D<float> near_left_top;
    hjh::Point3D<float> near_right_bottom;
    hjh::Point3D<float> near_right_top;

    hjh::Point3D<float> far_left_bottom;
    hjh::Point3D<float> far_left_top;
    hjh::Point3D<float> far_right_bottom;
    hjh::Point3D<float> far_right_top;

    switch( axis_to_scene )
    {
    // X-axis
    case 0:
        near_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );
        break;

    // Y-axis
    case 1:
        near_left_bottom  = hjh::Point3D<float>( min_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, min_y, max_z );
        near_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, max_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, max_y, max_z );
        break;

    // Z-axis
    case 2:
        near_left_bottom  = hjh::Point3D<float>( max_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( max_x, max_y, min_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, max_y, min_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, min_y, max_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( min_x, min_y, max_z );
        far_right_top    = hjh::Point3D<float>( min_x, max_y, max_z );
        break;
    }


    {
    pcl::PointXYZRGB temp;
    int rgb = ((int)255) << 16 | ((int)0) << 8 | ((int)0);
    temp.rgba = rgb;

    temp.x = near_left_bottom.x;
    temp.y = near_left_bottom.y;
    temp.z = near_left_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = near_left_top.x;
    temp.y = near_left_top.y;
    temp.z = near_left_top.z;
    cloud_ptr->push_back( temp );

    temp.x = near_right_bottom.x;
    temp.y = near_right_bottom.y;
    temp.z = near_right_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = near_right_top.x;
    temp.y = near_right_top.y;
    temp.z = near_right_top.z;
    cloud_ptr->push_back( temp );


    rgb = ((int)0) << 16 | ((int)255) << 8 | ((int)0);
    temp.rgba = rgb;

    temp.x = far_left_bottom.x;
    temp.y = far_left_bottom.y;
    temp.z = far_left_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = far_left_top.x;
    temp.y = far_left_top.y;
    temp.z = far_left_top.z;
    cloud_ptr->push_back( temp );

    temp.x = far_right_bottom.x;
    temp.y = far_right_bottom.y;
    temp.z = far_right_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = far_right_top.x;
    temp.y = far_right_top.y;
    temp.z = far_right_top.z;
    cloud_ptr->push_back( temp );
    }

    hjh::Plane<float> front_plane, back_plane;

    front_plane.update_equation( near_right_top, near_right_bottom, near_left_bottom );

    back_plane.update_equation( far_right_top, far_right_bottom, far_left_bottom );

    //--------------------------------------------------------------------------
    //  6 - for each point P on the image plane and C as the camera center:
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // 6.1 - offset: raycasting based on resolution (odd vs. even)
    //--------------------------------------------------------------------------

    float h_offset(0), v_offset(0);

    // add a half step if resolution is even
    if ( 0 == horizontal_resolution % 2 )
    {
        h_offset = 0.5f;
    }
    if ( 0 == vertical_resolution % 2 )
    {
        v_offset = 0.5f;
    }

    // counting the number of intersections
    int intersection_counter(0);

    // for all possible rays...
//#pragma omp parallel for
    for ( int h = 0; h<horizontal_resolution; ++h )
    {
        float horizontal_ray_angle = ( (float)(h-(horizontal_resolution/2)) + h_offset) * horizontal_delta_fov;

        for ( int v = 0; v<vertical_resolution; ++v )
        {
            float vertical_ray_angle = ( (float)(v-(vertical_resolution/2)) + v_offset) * vertical_delta_fov;

        //----------------------------------------------------------------------
        //  6.2 - find the intersection of CP with front and back planes: I_f and I_b
        //      - image plane could be at any distance (default = 1)
        //----------------------------------------------------------------------
            //..................................................................
            // 6.2.1 - line CP: ==> l
            //..................................................................

            // point p on the current ray
            hjh::Point3D<float> p;

            switch( axis_to_scene )
            {
            // X-axis
            case 0:
                p.x = 1.0f;
                p.y = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Y-axis
            case 1:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = 1.0f;
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Z-axis
            case 2:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                p.z = 1.0f;
                break;
            }

            // line passing through p and c(0,0,0)
            hjh::Point3D<float> c( 0, 0, 0);
            hjh::Line<float> l;
            l.update_equation( c, p );


            //..................................................................
            // 6.2.2 - intersection line CP with front and back planes: ==> pf, pb
            //..................................................................

            hjh::Point3D<float> pf, pb;

            if (
                    hjh::LINE_AND_PLANE_INTERSECTED == front_plane.intersection( l, pf )
                    &&
                    hjh::LINE_AND_PLANE_INTERSECTED == back_plane.intersection( l, pb )
               )
            {

                //..............................................................
                // 6.2.3 - check if the ray is starting inside the boundig box
                //..............................................................

                    if ( pf.x >= min_x && pf.x <= max_x
                         && pf.y >= min_y && pf.y <= max_y )
                    {

                //..............................................................
                // 6.2.4 - resolutin step on vector pf-pb: ==> res_step
                //..............................................................

                    hjh::Vector3D<float> fb(pf, pb);

                    // number of steps needed to reach from pf to pb by step size of voxel_size
                    int number_of_steps = static_cast<int>( fb.size() / voxel_size );

                    // to prevent devision by zero and also rounding up the number_of_steps
                    number_of_steps++;

                    float res_step = 1.0f / number_of_steps;

                //..............................................................
                // 6.2.5 - for t: 1 --> 0, t -= res_step: & NOT_INTERSECTED_YET
                //         check the point of ( t.pf + (1-t).pb ) if any pixel exists.
                //             - find the pixel :)
                //..............................................................

                    // to stop after first intersection per ray
                    bool not_intersected_yet = true;

                    // to check if the ray is located inside the bounding box
                    bool still_inside = true;

                    for ( int i = number_of_steps; i>=0 && not_intersected_yet && still_inside; --i )
                    {
                        // float counter
                        float t = i * res_step;

                        hjh::Point3D<float> target_point;
                        target_point.x = (t * pf.x) + ((1-t) * pb.x);
                        target_point.y = (t * pf.y) + ((1-t) * pb.y);
                        target_point.z = (t * pf.z) + ((1-t) * pb.z);

                        // check if the target point is sill inside the bounding box
                        if ( target_point.x < min_x || target_point.x > max_x
                             || target_point.y < min_y || target_point.y > max_y
                             || target_point.z < min_z || target_point.z > max_z)
                        {
                            still_inside = false;
                        }

                        if ( still_inside )
                        {
                            //..................................................
                            // check the pixel if any point exists in voxelized model
                            //      - get grid coorditanes
                            //      - get coordinates index at list
                            //      - get the point
                            //      - update the output image
                            //..................................................

                            Eigen::Vector3i grid_coordinates =
                                    vg_ptr->getGridCoordinates( target_point.x
                                                                , target_point.y
                                                                , target_point.z );

                            int index = vg_ptr->getCentroidIndexAt( grid_coordinates );

                            if ( -1 != index )
                            {
                                // intersected and it is done for the current ray.
                                not_intersected_yet = false;

                                intersection_counter++;

                                // updating the output image
                                pcl::PointXYZRGB p_xyzrgb = cloud_ptr->points.at( index );
                                cv::Vec3b pixel;
                                // BGR format
                                pixel[0] = (int)p_xyzrgb.b;
                                pixel[1] = (int)p_xyzrgb.g;
                                pixel[2] = (int)p_xyzrgb.r;

                                switch( axis_to_scene )
                                {
                                // X-axis same as Z-axis
                                case 0:
                                    image.at<cv::Vec3b>( (vertical_resolution-1) - v
                                                        , (horizontal_resolution-1) - h) = pixel;
                                    break;

                                // Y-axis
                                case 1:
                                    image.at<cv::Vec3b>( (vertical_resolution-1) - v
                                                         , h ) = pixel;
                                    break;

                                // Z-axis same as X-axis
                                case 2:
                                    image.at<cv::Vec3b>( (vertical_resolution-1) - v
                                                        , (horizontal_resolution-1) - h) = pixel;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // next ray
            vertical_ray_angle += vertical_delta_fov;
        }

        // next ray
        horizontal_ray_angle += horizontal_delta_fov;
    }


//    std::cout
//            << "\nnum intersections:  " << intersection_counter
//            << "\nnum points of grid: " << cloud_ptr->size()
//            << "\ndiff: " << intersection_counter - (int)cloud_ptr->size()
//            << std::endl;


    vg_ptr.reset();

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &image
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size
        , const FuseColorData &fd )
{
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //
    //  (   0 ) preparing containers...
    //
    //  (   I ) calculate the focal length based on the maximum FOV (h or v).
    //  (  II ) define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //  ( III ) per point Pi in the source point cloud, intersect the Pi and Phi.
    //  (  IV ) update the pixel-independent image, Ii, based on preceding result.
    //  (   V ) update the pixels of the output image, Io, according to Ii data.
    //
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //  (   0 ) preparing containers...
    //      - resulting-image resolution
    //      - create output image (RGB image)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == desired_rows || 0 == desired_cols )
    {
        calculate_raycasted_image_resolution(
                    horizontal_fov
                    , vertical_fov
                    , voxel_size
                    ,horizontal_resolution
                    , vertical_resolution );
    }

    if ( 0 != desired_rows ) vertical_resolution = desired_rows;
    if ( 0 != desired_cols ) horizontal_resolution = desired_cols;

    image = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_8UC3 );

    //--------------------------------------------------------------------------
    //  (   I ) calculate the focal length based on the maximum FOV (h or v).
    //--------------------------------------------------------------------------

    // max FOV in degree
    float max_FOV = std::max( horizontal_fov, vertical_fov );
    float focal_length = 1 / ( std::tan( hjh::degree2radian( max_FOV / 2) ) );


    //--------------------------------------------------------------------------
    //  (  II ) define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //--------------------------------------------------------------------------

    hjh::pcd2img::RayIntersectionOnImagePlane
            sample_intersection( focal_length, axis_to_scene );

    //--------------------------------------------------------------------------
    //  ( III ) per point Pi in the source point cloud, intersect the Pi and Phi.
    //  (  IV ) update the pixel-independent image, Ii, based on preceding result.
    //--------------------------------------------------------------------------

    // container (Ii)
    std::vector<hjh::pcd2img::RayIntersectionOnImagePlane> intersections;

//#pragma omp parallel for
    for ( int i=0; i<(int)(cloud_ptr->points.size()); ++i )
    {
        sample_intersection.intersect_with_ray( cloud_ptr->points[i] );
        //std::cout << "\n" << sample_intersection << std::endl;

        intersections.push_back( sample_intersection );
    }

    //--------------------------------------------------------------------------
    //  (   V ) update the pixels of the output image, Io, according to Ii data.
    //
    //  per pixel, first, check the closetst distance by which the pixel is being
    //  updated. There can be 3 different situations:
    //      1 - new_distance <= (1 - TOLERANCE) * last_closest_distance,
    //      2 - (1 - TOLERANCE) * last_closest_distance
    //              < new_distance <=
    //              (1 + TOLERANCE) * last_closet_distance
    //      3 - (1 + TOLERANCE) * last_closest_distance > new_distance
    //
    //      for situation (1) update the target pixel regardless of its last data.
    //      for case (2) fuse the new data into the target pixel.
    //      finally, for case (3), do not update the target pixel and discard the
    //      new data.
    //
    //      updating (or fusing) the target pixel:
    //          - if number-of-involved is zero: update the number and also the
    //          target data.
    //          - else, update the number and data by fusing the new data into
    //          the existing one.
    //          - to update data (even fusing) apply the effect of each pixel not
    //          only to the corresponding target pixel, but also on its neighbours
    //          based on the pattern bellow:
    //                                  1   4   1
    //                                  4   80  4
    //                                  1   4   1
    //          the total weight is 32 and the weight for the direct target is
    //          20 (62.5%).
    //
    //--------------------------------------------------------------------------

    // .........................................................................
    //  creating the FuseColorData object
    // .........................................................................
    hjh::pcd2img::FuseColorData color_data( image );
    color_data.set_tolerance( fd.get_tolerance() );
    color_data.set_direct_pixel_weight( fd.get_direct_pixel_weight() );
    color_data.set_plus_neighbours_weight( fd.get_plus_neighbours_weight() );
    color_data.set_cross_neighbours_weight( fd.get_cross_neighbours_weight() );
    color_data.set_flag_criterion_factor( fd.get_flag_criterion_factor() );
    color_data.set_flag_affect_neighbours( fd.get_flag_affect_neighbours() );
    color_data.set_flag_only_empty_neighbours( fd.get_flag_only_empty_neighbours() );
    // .........................................................................


    // loop over all points
//#pragma omp parallel for
    for ( std::vector<hjh::pcd2img::RayIntersectionOnImagePlane>::iterator
          it = intersections.begin();
          it != intersections.end(); ++it )
    {
        // the affected pixel (range mapping)
        int pixel_x( 0 );
        int pixel_y( 0 );
        int max_resolution = std::max( horizontal_resolution, vertical_resolution );
        float offset_x = float(horizontal_resolution) /  max_resolution;
        float offset_y = float(vertical_resolution) /  max_resolution;
        pixel_x = (int)(((it->get_col() + offset_x)/(2*offset_x)) * horizontal_resolution + 0.5f);
        pixel_y = (int)(((it->get_row() + offset_y)/(2*offset_y)) * vertical_resolution + 0.5f);

        // check boundaries (inside frame)
        if ( pixel_x > 0 && pixel_x < image.cols-1
             &&
             pixel_y > 0 && pixel_y < image.rows-1 )
        {
                // pixel effect
                cv::Vec3b color = it->get_color_BGR();

                if (!color_data.fuse_new_pixel( pixel_y, pixel_x, color, it->get_d() ) )
                {
                    std::cout << "\n\nERROR..." << std::endl;
                }
        }
    }

    color_data.get_target_image().copyTo( image );

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_depth_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
        pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size )
{
    //--------------------------------------------------------------------------
    // 1 - resulting-image resolution
    // 2 - create output image (depth image)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == desired_rows || 0 == desired_cols )
    {
        calculate_raycasted_image_resolution( horizontal_fov, vertical_fov, voxel_size
                                              ,horizontal_resolution, vertical_resolution );
    }

    if ( 0 != desired_rows ) vertical_resolution = desired_rows;
    if ( 0 != desired_cols ) horizontal_resolution = desired_cols;

    dimage = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_16UC1 );

    //--------------------------------------------------------------------------
    // 3 - calculate delta fov's
    //--------------------------------------------------------------------------

    // in degree
    double horizontal_delta_fov = horizontal_fov / horizontal_resolution;
    double vertical_delta_fov = vertical_fov / vertical_resolution;

    //--------------------------------------------------------------------------
    // 4 - voxelizing the 3D model
    //--------------------------------------------------------------------------

    boost::shared_ptr< pcl::VoxelGrid<pcl::PointXYZ> > vg_ptr( new pcl::VoxelGrid<pcl::PointXYZ>() );

    // set the flag ON for fast access
    vg_ptr->setSaveLeafLayout( true );

    vg_ptr->setInputCloud( cloud_ptr );
    vg_ptr->setLeafSize( voxel_size, voxel_size, voxel_size );
    pcl::PointCloud<pcl::PointXYZ>::Ptr voxelized_model( new pcl::PointCloud<pcl::PointXYZ>() );
    vg_ptr->filter( *voxelized_model );

    // swap the source cloud with the voxelizing result
    voxelized_model.swap( cloud_ptr );

//    std::vector<int> indexes = vg_ptr->getLeafLayout();

//    std::cout << "points: " << cloud_ptr->size() << std::endl;
//    std::cout << "data: " << indexes.size() << std::endl;

    //--------------------------------------------------------------------------
    // * - coordinates of min and max indexes
    //--------------------------------------------------------------------------

    Eigen::Vector3i mins = vg_ptr->getMinBoxCoordinates();
    Eigen::Vector3i maxs = vg_ptr->getMaxBoxCoordinates();
    Eigen::Vector3f leaf_size = vg_ptr->getLeafSize();

    float min_x = mins[0] * leaf_size[0];
    float min_y = mins[1] * leaf_size[1];
    float min_z = mins[2] * leaf_size[2];

    float max_x = maxs[0] * leaf_size[0];
    float max_y = maxs[1] * leaf_size[1];
    float max_z = maxs[2] * leaf_size[2];


    //--------------------------------------------------------------------------
    //  5 - find the front and back planes:
    //      - find the 4 corner points for each plane
    //      - create planes based on any desired but consistent 3 points
    //--------------------------------------------------------------------------

    hjh::Point3D<float> near_left_bottom;
    hjh::Point3D<float> near_left_top;
    hjh::Point3D<float> near_right_bottom;
    hjh::Point3D<float> near_right_top;

    hjh::Point3D<float> far_left_bottom;
    hjh::Point3D<float> far_left_top;
    hjh::Point3D<float> far_right_bottom;
    hjh::Point3D<float> far_right_top;

    switch( axis_to_scene )
    {
    // X-axis
    case 0:
        near_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );
        break;

    // Y-axis
    case 1:
        near_left_bottom  = hjh::Point3D<float>( min_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, min_y, max_z );
        near_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, max_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, max_y, max_z );
        break;

    // Z-axis
    case 2:
        near_left_bottom  = hjh::Point3D<float>( max_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( max_x, max_y, min_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, max_y, min_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, min_y, max_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( min_x, min_y, max_z );
        far_right_top    = hjh::Point3D<float>( min_x, max_y, max_z );
        break;
    }


    {
    pcl::PointXYZ temp;
    temp.x = near_left_bottom.x;
    temp.y = near_left_bottom.y;
    temp.z = near_left_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = near_left_top.x;
    temp.y = near_left_top.y;
    temp.z = near_left_top.z;
    cloud_ptr->push_back( temp );

    temp.x = near_right_bottom.x;
    temp.y = near_right_bottom.y;
    temp.z = near_right_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = near_right_top.x;
    temp.y = near_right_top.y;
    temp.z = near_right_top.z;
    cloud_ptr->push_back( temp );


    temp.x = far_left_bottom.x;
    temp.y = far_left_bottom.y;
    temp.z = far_left_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = far_left_top.x;
    temp.y = far_left_top.y;
    temp.z = far_left_top.z;
    cloud_ptr->push_back( temp );

    temp.x = far_right_bottom.x;
    temp.y = far_right_bottom.y;
    temp.z = far_right_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = far_right_top.x;
    temp.y = far_right_top.y;
    temp.z = far_right_top.z;
    cloud_ptr->push_back( temp );
    }

    hjh::Plane<float> front_plane, back_plane;

    front_plane.update_equation( near_right_top, near_right_bottom, near_left_bottom );

    back_plane.update_equation( far_right_top, far_right_bottom, far_left_bottom );

    //--------------------------------------------------------------------------
    //  6 - for each point P on the image plane and C as the camera center:
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // 6.1 - offset: raycasting based on resolution (odd vs. even)
    //--------------------------------------------------------------------------

    float h_offset(0), v_offset(0);

    // add a half step if resolution is even
    if ( 0 == horizontal_resolution % 2 )
    {
        h_offset = 0.5f;
    }
    if ( 0 == vertical_resolution % 2 )
    {
        v_offset = 0.5f;
    }

    // counting the number of intersections
    int intersection_counter(0);

    // for all possible rays...
//#pragma omp parallel for
    for ( int h = 0; h<horizontal_resolution; ++h )
    {
        float horizontal_ray_angle = ( (float)(h-(horizontal_resolution/2)) + h_offset) * horizontal_delta_fov;

        for ( int v = 0; v<vertical_resolution; ++v )
        {
            float vertical_ray_angle = ( (float)(v-(vertical_resolution/2)) + v_offset) * vertical_delta_fov;

        //----------------------------------------------------------------------
        //  6.2 - find the intersection of CP with front and back planes: I_f and I_b
        //      - image plane could be at any distance (default = 1)
        //----------------------------------------------------------------------
            //..................................................................
            // 6.2.1 - line CP: ==> l
            //..................................................................

            // point p on the current ray
            hjh::Point3D<float> p;

            switch( axis_to_scene )
            {
            // X-axis
            case 0:
                p.x = 1.0f;
                p.y = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Y-axis
            case 1:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = 1.0f;
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Z-axis
            case 2:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                p.z = 1.0f;
                break;
            }

            // line passing through p and c(0,0,0)
            hjh::Point3D<float> c( 0, 0, 0);
            hjh::Line<float> l;
            l.update_equation( c, p );


            //..................................................................
            // 6.2.2 - intersection line CP with front and back planes: ==> pf, pb
            //..................................................................

            hjh::Point3D<float> pf, pb;

            if (
                    hjh::LINE_AND_PLANE_INTERSECTED == front_plane.intersection( l, pf )
                    &&
                    hjh::LINE_AND_PLANE_INTERSECTED == back_plane.intersection( l, pb )
               )
            {

                //..............................................................
                // 6.2.3 - check if the ray is starting inside the boundig box
                //..............................................................

                    if ( pf.x >= min_x && pf.x <= max_x
                         && pf.y >= min_y && pf.y <= max_y )
                    {

                //..............................................................
                // 6.2.4 - resolutin step on vector pf-pb: ==> res_step
                //..............................................................

                    hjh::Vector3D<float> fb(pf, pb);

                    // number of steps needed to reach from pf to pb by step size of voxel_size
                    int number_of_steps = static_cast<int>( fb.size() / voxel_size );

                    // to prevent devision by zero and also rounding up the number_of_steps
                    number_of_steps++;

                    float res_step = 1.0f / number_of_steps;

                //..............................................................
                // 6.2.5 - for t: 1 --> 0, t -= res_step: & NOT_INTERSECTED_YET
                //         check the point of ( t.pf + (1-t).pb ) if any pixel exists.
                //             - find the pixel :)
                //..............................................................

                    // to stop after first intersection per ray
                    bool not_intersected_yet = true;

                    // to check if the ray is located inside the bounding box
                    bool still_inside = true;

                    for ( int i = number_of_steps; i>=0 && not_intersected_yet && still_inside; --i )
                    {
                        // float counter
                        float t = i * res_step;

                        hjh::Point3D<float> target_point;
                        target_point.x = (t * pf.x) + ((1-t) * pb.x);
                        target_point.y = (t * pf.y) + ((1-t) * pb.y);
                        target_point.z = (t * pf.z) + ((1-t) * pb.z);

                        // check if the target point is sill inside the bounding box
                        if ( target_point.x < min_x || target_point.x > max_x
                             || target_point.y < min_y || target_point.y > max_y
                             || target_point.z < min_z || target_point.z > max_z)
                        {
                            still_inside = false;
                        }

                        if ( still_inside )
                        {
                            //..................................................
                            // check the pixel if any point exists in voxelized model
                            //      - get grid coorditanes
                            //      - get coordinates index at list
                            //      - get the point
                            //      - update the output dimage
                            //..................................................

                            Eigen::Vector3i grid_coordinates =
                                    vg_ptr->getGridCoordinates( target_point.x
                                                                , target_point.y
                                                                , target_point.z );

                            int index = vg_ptr->getCentroidIndexAt( grid_coordinates );

                            if ( -1 != index )
                            {
                                // intersected and it is done for the current ray.
                                not_intersected_yet = false;

                                intersection_counter++;

                                // updating the output dimage
                                pcl::PointXYZ p_xyz = cloud_ptr->points.at( index );

                                // updating the output depth image
                                uint16_t d( 0 );
                                double distance_to_camera_center_mm( 0 )
                                        , distance_to_camera_plane_mm( 0 );

                                // distace to the center (0,0,0)
                                distance_to_camera_center_mm =
                                        std::sqrt(
                                            (p_xyz.x * p_xyz.x)
                                            + (p_xyz.y * p_xyz.y)
                                            + (p_xyz.z * p_xyz.z)
                                            );

                                // distance to the camera plane
                                distance_to_camera_plane_mm = std::abs(
                                        distance_to_camera_center_mm
                                        * 1000  // millimeters converted to meters
                                        * std::cos( hjh::degree2radian( std::abs( horizontal_ray_angle ) ) ) // horizontal alignment
                                        * std::cos( hjh::degree2radian( std::abs( vertical_ray_angle ) ) )   // vertical alignment
                                        );

                                // in uint16_t format
                                d = static_cast<uint16_t>( distance_to_camera_plane_mm );


                                switch( axis_to_scene )
                                {
                                // X-axis
                                case 0:
                                    dimage.at<uint16_t>( (vertical_resolution-1) - v
                                                        , (horizontal_resolution-1) - h) = d;
                                    break;

                                // Y-axis
                                case 1:
                                    dimage.at<uint16_t>( (vertical_resolution-1) - v
                                                         , h ) = d;
                                    break;

                                // Z-axis
                                case 2:
                                    dimage.at<uint16_t>( v
                                                        , h) = d;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // next ray
            vertical_ray_angle += vertical_delta_fov;
        }

        // next ray
        horizontal_ray_angle += horizontal_delta_fov;
    }


//    std::cout
//            << "\nnum intersections:  " << intersection_counter
//            << "\nnum points of grid: " << cloud_ptr->size()
//            << "\ndiff: " << intersection_counter - (int)cloud_ptr->size()
//            << std::endl;


    vg_ptr.reset();

    return 0;
}


int
convert_pointcloud_to_depth_image_RAYCASTING_FROM_CAMERA_TO_MODEL(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size )
{
    //--------------------------------------------------------------------------
    // 1 - resulting-image resolution
    // 2 - create output image (depth image)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == desired_rows || 0 == desired_cols )
    {
        calculate_raycasted_image_resolution( horizontal_fov, vertical_fov, voxel_size
                                              ,horizontal_resolution, vertical_resolution );
    }

    if ( 0 != desired_rows ) vertical_resolution = desired_rows;
    if ( 0 != desired_cols ) horizontal_resolution = desired_cols;

    dimage = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_16UC1 );

    //--------------------------------------------------------------------------
    // 3 - calculate delta fov's
    //--------------------------------------------------------------------------

    // in degree
    double horizontal_delta_fov = horizontal_fov / horizontal_resolution;
    double vertical_delta_fov = vertical_fov / vertical_resolution;

    //--------------------------------------------------------------------------
    // 4 - voxelizing the 3D model
    //--------------------------------------------------------------------------

    boost::shared_ptr< pcl::VoxelGrid<pcl::PointXYZRGB> > vg_ptr( new pcl::VoxelGrid<pcl::PointXYZRGB>() );

    // set the flag ON for fast access
    vg_ptr->setSaveLeafLayout( true );

    vg_ptr->setInputCloud( cloud_ptr );
    vg_ptr->setLeafSize( voxel_size, voxel_size, voxel_size );
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr voxelized_model( new pcl::PointCloud<pcl::PointXYZRGB>() );
    vg_ptr->filter( *voxelized_model );

    // swap the source cloud with the voxelizing result
    voxelized_model.swap( cloud_ptr );

//    std::vector<int> indexes = vg_ptr->getLeafLayout();

//    std::cout << "points: " << cloud_ptr->size() << std::endl;
//    std::cout << "data: " << indexes.size() << std::endl;

    //--------------------------------------------------------------------------
    // * - coordinates of min and max indexes
    //--------------------------------------------------------------------------

    Eigen::Vector3i mins = vg_ptr->getMinBoxCoordinates();
    Eigen::Vector3i maxs = vg_ptr->getMaxBoxCoordinates();
    Eigen::Vector3f leaf_size = vg_ptr->getLeafSize();

    float min_x = mins[0] * leaf_size[0];
    float min_y = mins[1] * leaf_size[1];
    float min_z = mins[2] * leaf_size[2];

    float max_x = maxs[0] * leaf_size[0];
    float max_y = maxs[1] * leaf_size[1];
    float max_z = maxs[2] * leaf_size[2];


    //--------------------------------------------------------------------------
    //  5 - find the front and back planes:
    //      - find the 4 corner points for each plane
    //      - create planes based on any desired but consistent 3 points
    //--------------------------------------------------------------------------

    hjh::Point3D<float> near_left_bottom;
    hjh::Point3D<float> near_left_top;
    hjh::Point3D<float> near_right_bottom;
    hjh::Point3D<float> near_right_top;

    hjh::Point3D<float> far_left_bottom;
    hjh::Point3D<float> far_left_top;
    hjh::Point3D<float> far_right_bottom;
    hjh::Point3D<float> far_right_top;

    switch( axis_to_scene )
    {
    // X-axis
    case 0:
        near_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );
        break;

    // Y-axis
    case 1:
        near_left_bottom  = hjh::Point3D<float>( min_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, min_y, max_z );
        near_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, max_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, max_y, max_z );
        break;

    // Z-axis
    case 2:
        near_left_bottom  = hjh::Point3D<float>( max_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( max_x, max_y, min_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, max_y, min_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, min_y, max_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( min_x, min_y, max_z );
        far_right_top    = hjh::Point3D<float>( min_x, max_y, max_z );
        break;
    }


    {
    pcl::PointXYZRGB temp;
    temp.x = near_left_bottom.x;
    temp.y = near_left_bottom.y;
    temp.z = near_left_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = near_left_top.x;
    temp.y = near_left_top.y;
    temp.z = near_left_top.z;
    cloud_ptr->push_back( temp );

    temp.x = near_right_bottom.x;
    temp.y = near_right_bottom.y;
    temp.z = near_right_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = near_right_top.x;
    temp.y = near_right_top.y;
    temp.z = near_right_top.z;
    cloud_ptr->push_back( temp );


    temp.x = far_left_bottom.x;
    temp.y = far_left_bottom.y;
    temp.z = far_left_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = far_left_top.x;
    temp.y = far_left_top.y;
    temp.z = far_left_top.z;
    cloud_ptr->push_back( temp );

    temp.x = far_right_bottom.x;
    temp.y = far_right_bottom.y;
    temp.z = far_right_bottom.z;
    cloud_ptr->push_back( temp );

    temp.x = far_right_top.x;
    temp.y = far_right_top.y;
    temp.z = far_right_top.z;
    cloud_ptr->push_back( temp );
    }

    hjh::Plane<float> front_plane, back_plane;

    front_plane.update_equation( near_right_top, near_right_bottom, near_left_bottom );

    back_plane.update_equation( far_right_top, far_right_bottom, far_left_bottom );

    //--------------------------------------------------------------------------
    //  6 - for each point P on the image plane and C as the camera center:
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // 6.1 - offset: raycasting based on resolution (odd vs. even)
    //--------------------------------------------------------------------------

    float h_offset(0), v_offset(0);

    // add a half step if resolution is even
    if ( 0 == horizontal_resolution % 2 )
    {
        h_offset = 0.5f;
    }
    if ( 0 == vertical_resolution % 2 )
    {
        v_offset = 0.5f;
    }

    // counting the number of intersections
    int intersection_counter(0);

    // for all possible rays...
//#pragma omp parallel for
    for ( int h = 0; h<horizontal_resolution; ++h )
    {
        float horizontal_ray_angle = ( (float)(h-(horizontal_resolution/2)) + h_offset) * horizontal_delta_fov;

        for ( int v = 0; v<vertical_resolution; ++v )
        {
            float vertical_ray_angle = ( (float)(v-(vertical_resolution/2)) + v_offset) * vertical_delta_fov;

        //----------------------------------------------------------------------
        //  6.2 - find the intersection of CP with front and back planes: I_f and I_b
        //      - image plane could be at any distance (default = 1)
        //----------------------------------------------------------------------
            //..................................................................
            // 6.2.1 - line CP: ==> l
            //..................................................................

            // point p on the current ray
            hjh::Point3D<float> p;

            switch( axis_to_scene )
            {
            // X-axis
            case 0:
                p.x = 1.0f;
                p.y = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Y-axis
            case 1:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = 1.0f;
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Z-axis
            case 2:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                p.z = 1.0f;
                break;
            }

            // line passing through p and c(0,0,0)
            hjh::Point3D<float> c( 0, 0, 0);
            hjh::Line<float> l;
            l.update_equation( c, p );


            //..................................................................
            // 6.2.2 - intersection line CP with front and back planes: ==> pf, pb
            //..................................................................

            hjh::Point3D<float> pf, pb;

            if (
                    hjh::LINE_AND_PLANE_INTERSECTED == front_plane.intersection( l, pf )
                    &&
                    hjh::LINE_AND_PLANE_INTERSECTED == back_plane.intersection( l, pb )
               )
            {

                //..............................................................
                // 6.2.3 - check if the ray is starting inside the boundig box
                //..............................................................

                    if ( pf.x >= min_x && pf.x <= max_x
                         && pf.y >= min_y && pf.y <= max_y )
                    {

                //..............................................................
                // 6.2.4 - resolutin step on vector pf-pb: ==> res_step
                //..............................................................

                    hjh::Vector3D<float> fb(pf, pb);

                    // number of steps needed to reach from pf to pb by step size of voxel_size
                    int number_of_steps = static_cast<int>( fb.size() / voxel_size );

                    // to prevent devision by zero and also rounding up the number_of_steps
                    number_of_steps++;

                    float res_step = 1.0f / number_of_steps;

                //..............................................................
                // 6.2.5 - for t: 1 --> 0, t -= res_step: & NOT_INTERSECTED_YET
                //         check the point of ( t.pf + (1-t).pb ) if any pixel exists.
                //             - find the pixel :)
                //..............................................................

                    // to stop after first intersection per ray
                    bool not_intersected_yet = true;

                    // to check if the ray is located inside the bounding box
                    bool still_inside = true;

                    for ( int i = number_of_steps; i>=0 && not_intersected_yet && still_inside; --i )
                    {
                        // float counter
                        float t = i * res_step;

                        hjh::Point3D<float> target_point;
                        target_point.x = (t * pf.x) + ((1-t) * pb.x);
                        target_point.y = (t * pf.y) + ((1-t) * pb.y);
                        target_point.z = (t * pf.z) + ((1-t) * pb.z);

                        // check if the target point is sill inside the bounding box
                        if ( target_point.x < min_x || target_point.x > max_x
                             || target_point.y < min_y || target_point.y > max_y
                             || target_point.z < min_z || target_point.z > max_z)
                        {
                            still_inside = false;
                        }

                        if ( still_inside )
                        {
                            //..................................................
                            // check the pixel if any point exists in voxelized model
                            //      - get grid coorditanes
                            //      - get coordinates index at list
                            //      - get the point
                            //      - update the output dimage
                            //..................................................

                            Eigen::Vector3i grid_coordinates =
                                    vg_ptr->getGridCoordinates( target_point.x
                                                                , target_point.y
                                                                , target_point.z );

                            int index = vg_ptr->getCentroidIndexAt( grid_coordinates );

                            if ( -1 != index )
                            {
                                // intersected and it is done for the current ray.
                                not_intersected_yet = false;

                                intersection_counter++;

                                // updating the output dimage
                                pcl::PointXYZRGB p_xyz = cloud_ptr->points.at( index );

                                // updating the output depth image
                                uint16_t d( 0 );
                                double distance_to_camera_center_mm( 0 )
                                        , distance_to_camera_plane_mm( 0 );

                                // distace to the center (0,0,0)
                                distance_to_camera_center_mm =
                                        std::sqrt(
                                            (p_xyz.x * p_xyz.x)
                                            + (p_xyz.y * p_xyz.y)
                                            + (p_xyz.z * p_xyz.z)
                                            );

                                // distance to the camera plane
                                distance_to_camera_plane_mm = std::abs(
                                        distance_to_camera_center_mm
                                        * 1000  // millimeters converted to meters
                                        * std::cos( hjh::degree2radian( std::abs( horizontal_ray_angle ) ) ) // horizontal alignment
                                        * std::cos( hjh::degree2radian( std::abs( vertical_ray_angle ) ) )   // vertical alignment
                                        );

                                // in uint16_t format
                                d = static_cast<uint16_t>( distance_to_camera_plane_mm );


                                switch( axis_to_scene )
                                {
                                // X-axis
                                case 0:
                                    dimage.at<uint16_t>( (vertical_resolution-1) - v
                                                        , (horizontal_resolution-1) - h) = d;
                                    break;

                                // Y-axis
                                case 1:
                                    dimage.at<uint16_t>( (vertical_resolution-1) - v
                                                         , h ) = d;
                                    break;

                                // Z-axis
                                case 2:
                                    dimage.at<uint16_t>( v
                                                        , h) = d;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // next ray
            vertical_ray_angle += vertical_delta_fov;
        }

        // next ray
        horizontal_ray_angle += horizontal_delta_fov;
    }


//    std::cout
//            << "\nnum intersections:  " << intersection_counter
//            << "\nnum points of grid: " << cloud_ptr->size()
//            << "\ndiff: " << intersection_counter - (int)cloud_ptr->size()
//            << std::endl;


    vg_ptr.reset();

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_depth_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        pcl::PointCloud<pcl::PointXYZ>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size
        , const FuseDepthData &fd )
{
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //
    //  (   0 ) preparing containers...
    //
    //  (   I ) calculate the focal length based on the maximum FOV (h or v).
    //  (  II ) define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //  ( III ) per point Pi in the source point cloud, intersect the Pi and Phi.
    //  (  IV ) update the pixel-independent image, Ii, based on preceding result.
    //  (   V ) update the pixels of the output image, Io, according to Ii data.
    //
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //  (   0 ) preparing containers...
    //      - resulting-image resolution
    //      - create output image (RGB image)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == desired_rows || 0 == desired_cols )
    {
        calculate_raycasted_image_resolution(
                    horizontal_fov
                    , vertical_fov
                    , voxel_size
                    ,horizontal_resolution
                    , vertical_resolution );
    }

    if ( 0 != desired_rows ) vertical_resolution = desired_rows;
    if ( 0 != desired_cols ) horizontal_resolution = desired_cols;

    dimage = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_16UC1 );

    //--------------------------------------------------------------------------
    //  (   I ) calculate the focal length based on the maximum FOV (h or v).
    //--------------------------------------------------------------------------

    // max FOV in degree
    float max_FOV = std::max( horizontal_fov, vertical_fov );
    float focal_length = 1 / ( std::tan( hjh::degree2radian( max_FOV / 2) ) );


    //--------------------------------------------------------------------------
    //  (  II ) define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //--------------------------------------------------------------------------

    hjh::pcd2img::RayIntersectionOnImagePlane
            sample_intersection( focal_length, axis_to_scene );
    sample_intersection.set_z_scale( 1000.0f );

    //--------------------------------------------------------------------------
    //  ( III ) per point Pi in the source point cloud, intersect the Pi and Phi.
    //  (  IV ) update the pixel-independent image, Ii, based on preceding result.
    //--------------------------------------------------------------------------

    // container (Ii)
    std::vector<hjh::pcd2img::RayIntersectionOnImagePlane> intersections;

    for ( int i=0; i<(int)(cloud_ptr->points.size()); ++i )
    {
        sample_intersection.intersect_with_ray( cloud_ptr->points[i] );

        intersections.push_back( sample_intersection );
    }

    //--------------------------------------------------------------------------
    //  (   V ) update the pixels of the output image, Io, according to Ii data.
    //
    //  per pixel, first, check the closetst distance by which the pixel is being
    //  updated. There can be 3 different situations:
    //      1 - new_distance <= (1 - TOLERANCE) * last_closest_distance,
    //      2 - (1 - TOLERANCE) * last_closest_distance
    //              < new_distance <=
    //              (1 + TOLERANCE) * last_closet_distance
    //      3 - (1 + TOLERANCE) * last_closest_distance > new_distance
    //
    //      for situation (1) update the target pixel regardless of its last data.
    //      for case (2) fuse the new data into the target pixel.
    //      finally, for case (3), do not update the target pixel and discard the
    //      new data.
    //
    //      updating (or fusing) the target pixel:
    //          - if number-of-involved is zero: update the number and also the
    //          target data.
    //          - else, update the number and data by fusing the new data into
    //          the existing one.
    //          - to update data (even fusing) apply the effect of each pixel not
    //          only to the corresponding target pixel, but also on its neighbours
    //          based on the pattern bellow:
    //                                  1   4   1
    //                                  4   80  4
    //                                  1   4  1
    //          the total weight is 32 and the weight for the direct target is
    //          20 (62.5%).
    //
    //--------------------------------------------------------------------------

    // .........................................................................
    //  creating the FuseColorData object
    // .........................................................................
    hjh::pcd2img::FuseDepthData depth_data( dimage );
    depth_data.set_tolerance( fd.get_tolerance() );
    depth_data.set_direct_pixel_weight( fd.get_direct_pixel_weight() );
    depth_data.set_plus_neighbours_weight( fd.get_plus_neighbours_weight() );
    depth_data.set_cross_neighbours_weight( fd.get_cross_neighbours_weight() );
    depth_data.set_flag_criterion_factor( fd.get_flag_criterion_factor() );
    depth_data.set_flag_affect_neighbours( fd.get_flag_affect_neighbours() );
    depth_data.set_flag_only_empty_neighbours( fd.get_flag_only_empty_neighbours() );
    // .........................................................................


    // loop over all points
    for ( std::vector<hjh::pcd2img::RayIntersectionOnImagePlane>::iterator
          it = intersections.begin();
          it != intersections.end(); ++it )
    {
        // the affected pixel (range mapping)
        int pixel_x( 0 );
        int pixel_y( 0 );
        int max_resolution = std::max( horizontal_resolution, vertical_resolution );
        float offset_x = float(horizontal_resolution) /  max_resolution;
        float offset_y = float(vertical_resolution) /  max_resolution;
        pixel_x = (int)(((it->get_col() + offset_x)/(2*offset_x)) * horizontal_resolution + 0.5f);
        pixel_y = (int)(((it->get_row() + offset_y)/(2*offset_y)) * vertical_resolution + 0.5f);

        // check boundaries (inside frame)
        if ( pixel_x > 0 && pixel_x < dimage.cols-1
             &&
             pixel_y > 0 && pixel_y < dimage.rows-1 )
        {
                // pixel effect
                uint16_t depth = it->get_z();

                if (!depth_data.fuse_new_pixel( pixel_y, pixel_x, depth, it->get_d() ) )
                {
                    std::cout << "\n\nERROR..." << std::endl;
                }
        }
    }

    depth_data.get_target_image().copyTo( dimage );

    return 0;
}

int
convert_pointcloud_to_depth_image_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , cv::Mat &dimage
        , double horizontal_fov, double vertical_fov
        , int axis_to_scene
        , unsigned int desired_rows
        , unsigned int desired_cols
        , double voxel_size
        , const FuseDepthData &fd )
{
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------
    //
    //  (   0 ) preparing containers...
    //
    //  (   I ) calculate the focal length based on the maximum FOV (h or v).
    //  (  II ) define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //  ( III ) per point Pi in the source point cloud, intersect the Pi and Phi.
    //  (  IV ) update the pixel-independent image, Ii, based on preceding result.
    //  (   V ) update the pixels of the output image, Io, according to Ii data.
    //
    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------


    //--------------------------------------------------------------------------
    //  (   0 ) preparing containers...
    //      - resulting-image resolution
    //      - create output image (RGB image)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == desired_rows || 0 == desired_cols )
    {
        calculate_raycasted_image_resolution(
                    horizontal_fov
                    , vertical_fov
                    , voxel_size
                    ,horizontal_resolution
                    , vertical_resolution );
    }

    if ( 0 != desired_rows ) vertical_resolution = desired_rows;
    if ( 0 != desired_cols ) horizontal_resolution = desired_cols;

    dimage = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_16UC1 );

    //--------------------------------------------------------------------------
    //  (   I ) calculate the focal length based on the maximum FOV (h or v).
    //--------------------------------------------------------------------------

    // max FOV in degree
    float max_FOV = std::max( horizontal_fov, vertical_fov );
    float focal_length = 1 / ( std::tan( hjh::degree2radian( max_FOV / 2) ) );

    //--------------------------------------------------------------------------
    //  (  II ) define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //--------------------------------------------------------------------------

    hjh::pcd2img::RayIntersectionOnImagePlane
            sample_intersection( focal_length, axis_to_scene );
    sample_intersection.set_z_scale( 1000.0f );

    //--------------------------------------------------------------------------
    //  ( III ) per point Pi in the source point cloud, intersect the Pi and Phi.
    //  (  IV ) update the pixel-independent image, Ii, based on preceding result.
    //--------------------------------------------------------------------------

    // container (Ii)
    std::vector<hjh::pcd2img::RayIntersectionOnImagePlane> intersections;

    for ( int i=0; i<(int)(cloud_ptr->points.size()); ++i )
    {
        sample_intersection.intersect_with_ray( cloud_ptr->points[i] );

        intersections.push_back( sample_intersection );
    }

    //--------------------------------------------------------------------------
    //  (   V ) update the pixels of the output image, Io, according to Ii data.
    //
    //  per pixel, first, check the closetst distance by which the pixel is being
    //  updated. There can be 3 different situations:
    //      1 - new_distance <= (1 - TOLERANCE) * last_closest_distance,
    //      2 - (1 - TOLERANCE) * last_closest_distance
    //              < new_distance <=
    //              (1 + TOLERANCE) * last_closet_distance
    //      3 - (1 + TOLERANCE) * last_closest_distance > new_distance
    //
    //      for situation (1) update the target pixel regardless of its last data.
    //      for case (2) fuse the new data into the target pixel.
    //      finally, for case (3), do not update the target pixel and discard the
    //      new data.
    //
    //      updating (or fusing) the target pixel:
    //          - if number-of-involved is zero: update the number and also the
    //          target data.
    //          - else, update the number and data by fusing the new data into
    //          the existing one.
    //          - to update data (even fusing) apply the effect of each pixel not
    //          only to the corresponding target pixel, but also on its neighbours
    //          based on the pattern bellow:
    //                                  1   4   1
    //                                  4   80  4
    //                                  1   4  1
    //          the total weight is 32 and the weight for the direct target is
    //          20 (62.5%).
    //
    //--------------------------------------------------------------------------

    // .........................................................................
    //  creating the FuseColorData object
    // .........................................................................
    hjh::pcd2img::FuseDepthData depth_data( dimage );
    depth_data.set_tolerance( fd.get_tolerance() );
    depth_data.set_direct_pixel_weight( fd.get_direct_pixel_weight() );
    depth_data.set_plus_neighbours_weight( fd.get_plus_neighbours_weight() );
    depth_data.set_cross_neighbours_weight( fd.get_cross_neighbours_weight() );
    depth_data.set_flag_criterion_factor( fd.get_flag_criterion_factor() );
    depth_data.set_flag_affect_neighbours( fd.get_flag_affect_neighbours() );
    depth_data.set_flag_only_empty_neighbours( fd.get_flag_only_empty_neighbours() );
    // .........................................................................


    // loop over all points
    for ( std::vector<hjh::pcd2img::RayIntersectionOnImagePlane>::iterator
          it = intersections.begin();
          it != intersections.end(); ++it )
    {
        // the affected pixel (range mapping)
        int pixel_x( 0 );
        int pixel_y( 0 );
        int max_resolution = std::max( horizontal_resolution, vertical_resolution );
        float offset_x = float(horizontal_resolution) /  max_resolution;
        float offset_y = float(vertical_resolution) /  max_resolution;
        pixel_x = (int)(((it->get_col() + offset_x)/(2*offset_x)) * horizontal_resolution + 0.5f);
        pixel_y = (int)(((it->get_row() + offset_y)/(2*offset_y)) * vertical_resolution + 0.5f);

        // check boundaries (inside frame)
        if ( pixel_x > 0 && pixel_x < dimage.cols-1
             &&
             pixel_y > 0 && pixel_y < dimage.rows-1 )
        {
                // pixel effect
                uint16_t depth = it->get_z();

                if (!depth_data.fuse_new_pixel( pixel_y, pixel_x, depth, it->get_d() ) )
                {
                    std::cout << "\n\nERROR..." << std::endl;
                }
        }
    }

    depth_data.get_target_image().copyTo( dimage );

    return 0;
}

//==============================================================================
//==============================================================================

inline
void
calculate_raycasted_image_resolution( double h_fov_deg      // in
                                      , double v_fov_deg    // in
                                      , double voxel_size   // in
                                      , int &h_res          // out
                                      , int &v_res )        // out
{
    // horizontal
    h_res = static_cast<int>( ( ( 2 * std::tan( hjh::degree2radian( h_fov_deg/2 ) ) ) / voxel_size ) + 0.5 );

    // vertical
    v_res = static_cast<int>( ( ( 2 * std::tan( hjh::degree2radian( v_fov_deg/2 ) ) ) / voxel_size ) + 0.5 );
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_square_pyramid_pack(
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , hjh::pcd2img::SquarePyramidPack &pack
        , hjh::pcd2img::PointCloudToImageConfig cfg
        , MODE mode )
{
    // check parameters

    // point cloud
    if (!cloud_ptr)
    {
        // BADAN throw an exception
        return -1;
    }

    // check fov
    if ( cfg.horizontal_FOV <= 0 || cfg.vertical_FOV <= 0 )
    {
        // BADAN throw an exception
        return -2;
    }

    // check the voxel size
    if ( cfg.voxel_size <= 0 )
    {
        // BADAN throw an exception
        return -3;
    }


    switch ( mode ) {
    case RAYCASTING_FROM_CAMERA_TO_MODEL:
        return convert_pointcloud_to_square_pyramid_RAYCASTING_FROM_CAMERA_TO_MODEL(
                    cloud_ptr, pack, cfg );
        break;

    case BACK_RAYCASTING_FROM_MODEL_TO_CAMERA:
        return convert_pointcloud_to_square_pyramid_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
                    cloud_ptr, pack, cfg );
        break;

    default:
        // BADAN throw an exception
        return -4;
        break;
    }

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_square_pyramid_RAYCASTING_FROM_CAMERA_TO_MODEL(
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , hjh::pcd2img::SquarePyramidPack &pack
        , hjh::pcd2img::PointCloudToImageConfig cfg)
{
    //--------------------------------------------------------------------------
    //  initializing output clouds
    //--------------------------------------------------------------------------

    if ( !pack.original_cloud_ptr )
    {
        pack.original_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(
                    new pcl::PointCloud<pcl::PointXYZRGB> );
    }

    if ( !pack.voxelized_cloud_ptr )
    {
        pack.voxelized_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(
                    new pcl::PointCloud<pcl::PointXYZRGB> );
    }

    //--------------------------------------------------------------------------
    // 0 - filtering the input cloud based on the FOV data
    //--------------------------------------------------------------------------

    if ( hjh::filter3D::fov_filtering( cloud_ptr, pack.original_cloud_ptr
                                  , cfg.horizontal_FOV, cfg.vertical_FOV
                                  , cfg.axis_toward_scene, true ) < 0 )
    {
        // BADAN todo throw an exception
        return -1;
    }

    //--------------------------------------------------------------------------
    // 1 - resulting-image resolution
    // 2 - create the output color/depth images
    // 3 - create the output point clouds
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == cfg.desired_image_size_rows || 0 == cfg.desired_image_size_cols )
    {
        calculate_raycasted_image_resolution( cfg.horizontal_FOV, cfg.vertical_FOV
                                              , cfg.voxel_size
                                              , horizontal_resolution, vertical_resolution );
    }

    if ( 0 != cfg.desired_image_size_rows ) vertical_resolution = cfg.desired_image_size_rows;
    if ( 0 != cfg.desired_image_size_cols ) horizontal_resolution = cfg.desired_image_size_cols;

    pack.color_image = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_8UC3);
    pack.depth_image = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_16UC1);


    //--------------------------------------------------------------------------
    // 3 - calculate delta fov's
    //--------------------------------------------------------------------------

    // in degree
    double horizontal_delta_fov = cfg.horizontal_FOV / horizontal_resolution;
    double vertical_delta_fov = cfg.vertical_FOV / vertical_resolution;

    //--------------------------------------------------------------------------
    // 4 - voxelizing the 3D model
    //--------------------------------------------------------------------------

    boost::shared_ptr< pcl::VoxelGrid<pcl::PointXYZRGB> > vg_ptr( new pcl::VoxelGrid<pcl::PointXYZRGB>() );

    // set the flag ON for fast access
    vg_ptr->setSaveLeafLayout( true );

    vg_ptr->setInputCloud( pack.original_cloud_ptr );
    vg_ptr->setLeafSize( cfg.voxel_size, cfg.voxel_size, cfg.voxel_size );
    pack.voxelized_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr( new pcl::PointCloud<pcl::PointXYZRGB>() );
    vg_ptr->filter( *(pack.voxelized_cloud_ptr) );

    //--------------------------------------------------------------------------
    // * - coordinates of min and max indexes
    //--------------------------------------------------------------------------

    Eigen::Vector3i mins = vg_ptr->getMinBoxCoordinates();
    Eigen::Vector3i maxs = vg_ptr->getMaxBoxCoordinates();
    Eigen::Vector3f leaf_size = vg_ptr->getLeafSize();

    float min_x = mins[0] * leaf_size[0];
    float min_y = mins[1] * leaf_size[1];
    float min_z = mins[2] * leaf_size[2];

    float max_x = maxs[0] * leaf_size[0];
    float max_y = maxs[1] * leaf_size[1];
    float max_z = maxs[2] * leaf_size[2];


    //--------------------------------------------------------------------------
    //  5 - find the front and back planes:
    //      - find the 4 corner points for each plane
    //      - create planes based on any desired but consistent 3 points
    //--------------------------------------------------------------------------

    hjh::Point3D<float> near_left_bottom;
    hjh::Point3D<float> near_left_top;
    hjh::Point3D<float> near_right_bottom;
    hjh::Point3D<float> near_right_top;

    hjh::Point3D<float> far_left_bottom;
    hjh::Point3D<float> far_left_top;
    hjh::Point3D<float> far_right_bottom;
    hjh::Point3D<float> far_right_top;

    switch( cfg.axis_toward_scene )
    {
    // X-axis
    case 0:
        near_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );
        break;

    // Y-axis
    case 1:
        near_left_bottom  = hjh::Point3D<float>( min_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( min_x, min_y, max_z );
        near_right_bottom = hjh::Point3D<float>( max_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( max_x, min_y, max_z );

        far_left_bottom  = hjh::Point3D<float>( min_x, max_y, min_z );
        far_left_top     = hjh::Point3D<float>( min_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( max_x, max_y, min_z );
        far_right_top    = hjh::Point3D<float>( max_x, max_y, max_z );
        break;

    // Z-axis
    case 2:
        near_left_bottom  = hjh::Point3D<float>( max_x, min_y, min_z );
        near_left_top     = hjh::Point3D<float>( max_x, max_y, min_z );
        near_right_bottom = hjh::Point3D<float>( min_x, min_y, min_z );
        near_right_top    = hjh::Point3D<float>( min_x, max_y, min_z );

        far_left_bottom  = hjh::Point3D<float>( max_x, min_y, max_z );
        far_left_top     = hjh::Point3D<float>( max_x, max_y, max_z );
        far_right_bottom = hjh::Point3D<float>( min_x, min_y, max_z );
        far_right_top    = hjh::Point3D<float>( min_x, max_y, max_z );
        break;
    }


    hjh::Plane<float> front_plane, back_plane;

    front_plane.update_equation( near_right_top, near_right_bottom, near_left_bottom );

    back_plane.update_equation( far_right_top, far_right_bottom, far_left_bottom );

    //--------------------------------------------------------------------------
    //  6 - for each point P on the image plane and C as the camera center:
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // 6.1 - offset: raycasting based on resolution (odd vs. even)
    //--------------------------------------------------------------------------

    float h_offset(0), v_offset(0);

    // add a half step if resolution is even
    if ( 0 == horizontal_resolution % 2 )
    {
        h_offset = 0.5f;
    }
    if ( 0 == vertical_resolution % 2 )
    {
        v_offset = 0.5f;
    }

    // counting the number of intersections
    int intersection_counter(0);

#pragma omp parallel for
    // for all possible rays...
    for ( int h = 0; h<horizontal_resolution; ++h )
    {
        float horizontal_ray_angle = ( (float)(h-(horizontal_resolution/2)) + h_offset) * horizontal_delta_fov;

        for ( int v = 0; v<vertical_resolution; ++v )
        {
            float vertical_ray_angle = ( (float)(v-(vertical_resolution/2)) + v_offset) * vertical_delta_fov;

        //----------------------------------------------------------------------
        //  6.2 - find the intersection of CP with front and back planes: I_f and I_b
        //      - image plane could be at any distance (default = 1)
        //----------------------------------------------------------------------
            //..................................................................
            // 6.2.1 - line CP: ==> l
            //..................................................................

            // point p on the current ray
            hjh::Point3D<float> p;

            switch( cfg.axis_toward_scene )
            {
            // X-axis
            case 0:
                p.x = 1.0f;
                p.y = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Y-axis
            case 1:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = 1.0f;
                p.z = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                break;

            // Z-axis
            case 2:
                p.x = std::tan( hjh::degree2radian( horizontal_ray_angle ) );
                p.y = std::tan( hjh::degree2radian( vertical_ray_angle ) );
                p.z = 1.0f;
                break;
            }

            // line passing through p and c(0,0,0)
            hjh::Point3D<float> c( 0, 0, 0);
            hjh::Line<float> l;
            l.update_equation( c, p );


            //..................................................................
            // 6.2.2 - intersection line CP with front and back planes: ==> pf, pb
            //..................................................................

            hjh::Point3D<float> pf, pb;

            if (
                    hjh::LINE_AND_PLANE_INTERSECTED == front_plane.intersection( l, pf )
                    &&
                    hjh::LINE_AND_PLANE_INTERSECTED == back_plane.intersection( l, pb )
               )
            {

                //..............................................................
                // 6.2.3 - check if the ray is starting inside the boundig box
                //..............................................................

                    if ( pf.x >= min_x && pf.x <= max_x
                         && pf.y >= min_y && pf.y <= max_y )
                    {

                //..............................................................
                // 6.2.4 - resolutin step on vector pf-pb: ==> res_step
                //..............................................................

                    hjh::Vector3D<float> fb(pf, pb);

                    // number of steps needed to reach from pf to pb by step size of voxel_size
                    int number_of_steps = static_cast<int>( fb.size() / cfg.voxel_size );

                    // to prevent devision by zero and also rounding up the number_of_steps
                    number_of_steps++;

                    float res_step = 1.0f / number_of_steps;

                //..............................................................
                // 6.2.5 - for t: 1 --> 0, t -= res_step: & NOT_INTERSECTED_YET
                //         check the point of ( t.pf + (1-t).pb ) if any pixel exists.
                //             - find the pixel :)
                //..............................................................

                    // to stop after first intersection per ray
                    bool not_intersected_yet = true;

                    // to check if the ray is located inside the bounding box
                    bool still_inside = true;

                    for ( int i = number_of_steps; i>=0 && not_intersected_yet && still_inside; --i )
                    {
                        // float counter
                        float t = i * res_step;

                        hjh::Point3D<float> target_point;
                        target_point.x = (t * pf.x) + ((1-t) * pb.x);
                        target_point.y = (t * pf.y) + ((1-t) * pb.y);
                        target_point.z = (t * pf.z) + ((1-t) * pb.z);

                        // check if the target point is still inside the bounding box
                        if ( target_point.x < min_x || target_point.x > max_x
                             || target_point.y < min_y || target_point.y > max_y
                             || target_point.z < min_z || target_point.z > max_z)
                        {
                            still_inside = false;
                        }

                        if ( still_inside )
                        {
                            //..................................................
                            // check the pixel if any point exists in voxelized model
                            //      - get grid coorditanes
                            //      - get coordinates index at list
                            //      - get the point
                            //      - update the output image
                            //..................................................

                            Eigen::Vector3i grid_coordinates =
                                    vg_ptr->getGridCoordinates( target_point.x
                                                                , target_point.y
                                                                , target_point.z );

                            int index = vg_ptr->getCentroidIndexAt( grid_coordinates );

                            if ( -1 != index )
                            {
                                // intersected and it is done for the current ray.
                                not_intersected_yet = false;

                                intersection_counter++;

                                // the target point
                                pcl::PointXYZRGB p_xyzrgb = pack.voxelized_cloud_ptr->points.at( index );

                                // updating the output color image
                                cv::Vec3b pixel_rgb;
                                // BGR format
                                pixel_rgb[0] = (int)p_xyzrgb.b;
                                pixel_rgb[1] = (int)p_xyzrgb.g;
                                pixel_rgb[2] = (int)p_xyzrgb.r;

                                switch( cfg.axis_toward_scene )
                                {
                                // X-axis same as Z-axis
                                case 0:
                                    pack.color_image.at<cv::Vec3b>( (vertical_resolution-1) - v
                                                        , (horizontal_resolution-1) - h) = pixel_rgb;
                                    break;

                                // Y-axis
                                case 1:
                                    pack.color_image.at<cv::Vec3b>( (vertical_resolution-1) - v
                                                         , h ) = pixel_rgb;
                                    break;

                                // Z-axis
                                case 2:
                                    pack.color_image.at<cv::Vec3b>( v
                                                        , h ) = pixel_rgb;
                                    break;
                                }

                                // updating the output depth image
                                uint16_t d( 0 );
                                double distance_to_camera_center_mm( 0 )
                                        , distance_to_camera_plane_mm( 0 );

                                // distace to the center (0,0,0)
                                distance_to_camera_center_mm =
                                        std::sqrt(
                                            (p_xyzrgb.x * p_xyzrgb.x)
                                            + (p_xyzrgb.y * p_xyzrgb.y)
                                            + (p_xyzrgb.z * p_xyzrgb.z)
                                            );

                                // distance to the camera plane
                                distance_to_camera_plane_mm = std::abs(
                                        distance_to_camera_center_mm
                                        * 1000  // millimeters converted to meters
                                        * std::cos( hjh::degree2radian( std::abs( horizontal_ray_angle ) ) ) // horizontal alignment
                                        * std::cos( hjh::degree2radian( std::abs( vertical_ray_angle ) ) )   // vertical alignment
                                        );

                                // in uint16_t format
                                d = static_cast<uint16_t>( distance_to_camera_plane_mm );


                                switch( cfg.axis_toward_scene )
                                {
                                // X-axis same as Z-axis
                                case 0:
                                    pack.depth_image.at<uint16_t>( (vertical_resolution-1) - v
                                                        , (horizontal_resolution-1) - h) = d;
                                    break;

                                // Y-axis
                                case 1:
                                    pack.depth_image.at<uint16_t>( (vertical_resolution-1) - v
                                                         , h ) = d;
                                    break;

                                // Z-axis
                                case 2:
                                    pack.depth_image.at<uint16_t>( v
                                                        , h) = d;
                                    break;
                                }


//                                switch( cfg.axis_toward_scene )
//                                {
//                                // X-axis same as Z-axis
//                                case 0:
//                                    distance_to_camera_plane_mm
//                                            = std::abs( p_xyzrgb.x )    // distance to the camera center
//                                            * 1000                      // in millimeters
//                                            * std::cos( hjh::degree2radian( std::abs( horizontal_ray_angle ) ) ) // horizontal alignment
//                                            * std::cos( hjh::degree2radian( std::abs( vertical_ray_angle ) ) ) // vertical alignment
//                                            ;
//                                    d = static_cast<uint16_t>( distance_to_camera_plane_mm );

//                                    pack.depth_image.at<uint16_t>( (vertical_resolution-1) - v
//                                                        , (horizontal_resolution-1) - h) = d;
//                                    break;

//                                // Y-axis
//                                case 1:
//                                    distance_to_camera_plane_mm
//                                            = std::abs( p_xyzrgb.y )    // distance to the camera center
//                                            * 1000                      // in millimeters
//                                            * std::cos( hjh::degree2radian( std::abs( horizontal_ray_angle ) ) ) // horizontal alignment
//                                            * std::cos( hjh::degree2radian( std::abs( vertical_ray_angle ) ) ) // vertical alignment
//                                            ;
//                                    d = static_cast<uint16_t>( distance_to_camera_plane_mm );
//                                    pack.depth_image.at<uint16_t>( (vertical_resolution-1) - v
//                                                         , h ) = d;
//                                    break;

//                                // Z-axis same as X-axis
//                                case 2:
//                                    distance_to_camera_plane_mm
//                                            = std::abs( p_xyzrgb.z )    // distance to the camera center
//                                            * 1000                      // in millimeters
//                                            * std::cos( hjh::degree2radian( std::abs( horizontal_ray_angle ) ) ) // horizontal alignment
//                                            * std::cos( hjh::degree2radian( std::abs( vertical_ray_angle ) ) ) // vertical alignment
//                                            ;
//                                    d = static_cast<uint16_t>( distance_to_camera_plane_mm );
//                                    pack.depth_image.at<uint16_t>( (vertical_resolution-1) - v
//                                                        , (horizontal_resolution-1) - h) = d;
//                                    break;
//                                }
                            }
                        }
                    }
                }
            }

            // next ray
            vertical_ray_angle += vertical_delta_fov;
        }

        // next ray
        horizontal_ray_angle += horizontal_delta_fov;
    }


    vg_ptr.reset();

    return 0;
}

//==============================================================================
//==============================================================================

int
convert_pointcloud_to_square_pyramid_BACK_RAYCASTING_FROM_MODEL_TO_CAMERA(
        const pcl::PointCloud<pcl::PointXYZRGB>::Ptr &cloud_ptr
        , hjh::pcd2img::SquarePyramidPack &pack
        , hjh::pcd2img::PointCloudToImageConfig cfg)
{
    //--------------------------------------------------------------------------
    //  0 - initializing output clouds
    //--------------------------------------------------------------------------

    if ( pack.original_cloud_ptr ) pack.original_cloud_ptr.reset();
    pack.original_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(
                new pcl::PointCloud<pcl::PointXYZRGB> );

    if ( pack.voxelized_cloud_ptr ) pack.voxelized_cloud_ptr.reset();
    pack.voxelized_cloud_ptr = pcl::PointCloud<pcl::PointXYZRGB>::Ptr(
                new pcl::PointCloud<pcl::PointXYZRGB> );

    //--------------------------------------------------------------------------
    // 1 - filtering the input cloud based on the FOV data (optional)
    //--------------------------------------------------------------------------

    if ( hjh::filter3D::fov_filtering( cloud_ptr, pack.original_cloud_ptr
                                  , cfg.horizontal_FOV, cfg.vertical_FOV
                                  , cfg.axis_toward_scene, true ) < 0 )
    {
        // BADAN todo throw an exception
        return -1;
    }

    //--------------------------------------------------------------------------
    //  2 - preparing containers...
    //      - resulting-image resolution
    //      - create output images (depth/color)
    //--------------------------------------------------------------------------

    // in pixels
    int horizontal_resolution(0), vertical_resolution(0);

    if ( 0 == cfg.desired_image_size_rows || 0 == cfg.desired_image_size_cols )
    {
        calculate_raycasted_image_resolution( cfg.horizontal_FOV, cfg.vertical_FOV
                                              , cfg.voxel_size
                                              , horizontal_resolution, vertical_resolution );
    }

    if ( 0 != cfg.desired_image_size_rows ) vertical_resolution = cfg.desired_image_size_rows;
    if ( 0 != cfg.desired_image_size_cols ) horizontal_resolution = cfg.desired_image_size_cols;

    pack.color_image = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_8UC3);
    pack.depth_image = cv::Mat::zeros( vertical_resolution, horizontal_resolution, CV_16UC1);


    //--------------------------------------------------------------------------
    //  3 - calculate the focal length based on the maximum FOV (h or v).
    //--------------------------------------------------------------------------

    // max FOV in degree
    float max_FOV = std::max( cfg.horizontal_FOV, cfg.vertical_FOV );
    float focal_length = 1 / ( std::tan( hjh::degree2radian( max_FOV / 2) ) );


    //--------------------------------------------------------------------------
    //  4 - define a (1+1) by (1+1) image plane, named Phi, infront of
    //          the camera center, C.
    //--------------------------------------------------------------------------

    hjh::pcd2img::RayIntersectionOnImagePlane
            sample_intersection( focal_length, cfg.axis_toward_scene );
    sample_intersection.set_z_scale( 1000.0f );

    //--------------------------------------------------------------------------
    //  5 - per point Pi in the source point cloud, intersect the Pi and Phi.
    //  6 - update the pixel-independent image based on the preceding result.
    //--------------------------------------------------------------------------

    // container (pixel-independent image)
    std::vector<hjh::pcd2img::RayIntersectionOnImagePlane> intersections;

//#pragma omp parallel for
    for ( int i=0; i<(int)(pack.original_cloud_ptr->points.size()); ++i )
    {
        sample_intersection.intersect_with_ray( pack.original_cloud_ptr->points[i] );
        intersections.push_back( sample_intersection );
    }

    //--------------------------------------------------------------------------
    //  7 - creating the FuseColorData object
    //  8 - creating the FuseDepthData object
    //--------------------------------------------------------------------------

    hjh::pcd2img::FuseColorData color_data( pack.color_image );
    color_data.set_tolerance( cfg.fuse_color_data.get_tolerance() );
    color_data.set_direct_pixel_weight( cfg.fuse_color_data.get_direct_pixel_weight() );
    color_data.set_plus_neighbours_weight( cfg.fuse_color_data.get_plus_neighbours_weight() );
    color_data.set_cross_neighbours_weight( cfg.fuse_color_data.get_cross_neighbours_weight() );
    color_data.set_flag_criterion_factor( cfg.fuse_color_data.get_flag_criterion_factor() );
    color_data.set_flag_affect_neighbours( cfg.fuse_color_data.get_flag_affect_neighbours() );
    color_data.set_flag_only_empty_neighbours( cfg.fuse_color_data.get_flag_only_empty_neighbours() );
    // .........................................................................

    hjh::pcd2img::FuseDepthData depth_data( pack.depth_image );
    depth_data.set_tolerance( cfg.fuse_depth_data.get_tolerance() );
    depth_data.set_direct_pixel_weight( cfg.fuse_depth_data.get_direct_pixel_weight() );
    depth_data.set_plus_neighbours_weight( cfg.fuse_depth_data.get_plus_neighbours_weight() );
    depth_data.set_cross_neighbours_weight( cfg.fuse_depth_data.get_cross_neighbours_weight() );
    depth_data.set_flag_criterion_factor( cfg.fuse_depth_data.get_flag_criterion_factor() );
    depth_data.set_flag_affect_neighbours( cfg.fuse_depth_data.get_flag_affect_neighbours() );
    depth_data.set_flag_only_empty_neighbours( cfg.fuse_depth_data.get_flag_only_empty_neighbours() );
    // .........................................................................


    // loop over all points
//#pragma omp parallel for
    for ( std::vector<hjh::pcd2img::RayIntersectionOnImagePlane>::iterator
          it = intersections.begin();
          it != intersections.end(); ++it )
    {
        // the affected pixel (range mapping)
        int pixel_x( 0 );
        int pixel_y( 0 );
        int max_resolution = std::max( horizontal_resolution, vertical_resolution );
        float offset_x = float(horizontal_resolution) /  max_resolution;
        float offset_y = float(vertical_resolution) /  max_resolution;
        pixel_x = (int)(((it->get_col() + offset_x)/(2*offset_x)) * horizontal_resolution + 0.5f);
        pixel_y = (int)(((it->get_row() + offset_y)/(2*offset_y)) * vertical_resolution + 0.5f);

        // check boundaries (inside frame)
        if ( pixel_x > 0 && pixel_x < pack.color_image.cols-1
             &&
             pixel_y > 0 && pixel_y < pack.color_image.rows-1 )
        {
                // pixel effect
                cv::Vec3b color = it->get_color_BGR();
                uint16_t depth = it->get_z();

                color_data.fuse_new_pixel( pixel_y, pixel_x, color, it->get_d() );
                depth_data.fuse_new_pixel( pixel_y, pixel_x, depth, it->get_d() );
        }
    }

    color_data.get_target_image().copyTo( pack.color_image );
    depth_data.get_target_image().copyTo( pack.depth_image );

    return 0;
}

//==============================================================================
//==============================================================================


} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
