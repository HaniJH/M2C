
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/FuseDepthData.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

//==============================================================================
//==============================================================================

FuseDepthData::FuseDepthData()
    : flag_criterion_factor( true )
    , tolerance( 0.005f )
    , plus_neighbours_weight( 4 )
    , cross_neighbours_weight( 1 )
    , direct_pixel_weight( 80 )
    , flag_affect_neighbours( true )
    , flag_only_empty_neighbours( true )
{
    cv::Mat temp_image = cv::Mat::zeros( 480, 640, CV_8UC3 );

    set_target_image( temp_image );
}

//==============================================================================
//==============================================================================

FuseDepthData::FuseDepthData( const cv::Mat &target_image_ )
    : flag_criterion_factor( true )
    , tolerance( 0.005f )
    , plus_neighbours_weight( 4 )
    , cross_neighbours_weight( 1 )
    , direct_pixel_weight( 80 )
    , flag_affect_neighbours( true )
    , flag_only_empty_neighbours( true )
{
    set_target_image( target_image_ );
}

//==============================================================================
//==============================================================================

FuseDepthData::FuseDepthData( int rows_, int cols_ )
    : flag_criterion_factor( true )
    , tolerance( 0.005f )
    , plus_neighbours_weight( 4 )
    , cross_neighbours_weight( 1 )
    , direct_pixel_weight( 80 )
    , flag_affect_neighbours( true )
    , flag_only_empty_neighbours( true )
{
    cv::Mat temp_image = cv::Mat::zeros( rows_, cols_, CV_16UC1 );

    set_target_image( temp_image );
}

//==============================================================================
//==============================================================================

FuseDepthData::~FuseDepthData()
{
    target_image.release();
    total_weight.release();
    last_updated_factor.release();
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<(std::ostream& os, const FuseDepthData& obj)
{
    os << "    target image (rows) : " << obj.target_image.rows << std::endl
       << "    target image (cols) : " << obj.target_image.cols << std::endl
       << "       criterion_factor : " << (obj.flag_criterion_factor?"ON":"OFF") << std::endl
       << "              tolerance : " << obj.tolerance << std::endl
       << " plus neighbours weight : " << obj.plus_neighbours_weight << std::endl
       << "cross neighbours weight : " << obj.cross_neighbours_weight << std::endl
       << "    direct pixel weight : " << obj.direct_pixel_weight
       ;
    return os;
}

//==============================================================================
//==============================================================================

//--------------------------------------------------------------------------
//  per pixel, first, check the closetst distance by which the pixel is being
//  updated. There can be 3 different situations:
//      1 - new_distance <= (1 - TOLERANCE) * last_closest_distance,
//      2 - (1 - TOLERANCE) * last_closest_distance
//              < new_distance <=
//              (1 + TOLERANCE) * last_closet_distance
//      3 - (1 + TOLERANCE) * last_closest_distance > new_distance
//
//      for situation (1) update the target pixel regardless of its last data.
//      for case (2) fuse the new data into the target pixel.
//      finally, for case (3), do not update the target pixel and discard the
//      new data.
//
//      updating (or fusing) the target pixel:
//          - if number-of-involved is zero: update the number and also the
//          target data.
//          - else, update the number and data by fusing the new data into
//          the existing one.
//          - to update data (even fusing) apply the effect of each pixel not
//          only to the corresponding target pixel, but also on its neighbours
//          based on the pattern bellow:
//                                  x   +   x
//                                  +   T   +
//                                  x   +   x
//
//              + : plus_neighbours_weight
//              x : cross_neighbours_weight
//              T : direct_pixel_weight (target)
//
//          the total weight is 32 and the weight for the direct target is
//          20 (62.5%).
//
//--------------------------------------------------------------------------

bool
FuseDepthData::fuse_new_pixel( int row_, int col_, uint16_t depth_, float factor_ )
{
    if ( target_image.empty() )
    {
        // BADAN ToDo throw an exception
        return false;
    }

    // new pixel status
    NEW_PIXEL_STATUS status = UNKNOWN;

    status = get_new_pixel_status( row_, col_, factor_ );

    //--------------------------------------------------------------------------
    // target pixel itself
    //--------------------------------------------------------------------------
    switch( status )
    {
    case UNKNOWN:
        // nothing to do, but indicate the error
        return false;

    case DISCARD:
        // nothing to do
        return true;

    case INTEGRATE:
        fuse_depth( row_, col_, direct_pixel_weight, depth_, false );
//        fuse_color( row_, col_, direct_pixel_weight, RED );
        break;

    case REPLACE:
        replace_depth( row_, col_, direct_pixel_weight, depth_, false );
//        replace_color( row_, col_, direct_pixel_weight, GREEN );
        break;
    }
    //--------------------------------------------------------------------------


    // update the factor
    update_last_updated_factor( row_, col_, factor_ );

    //--------------------------------------------------------------------------
    // neighbours
    //--------------------------------------------------------------------------
    if ( flag_affect_neighbours )
    {
        switch( is_pixel_on_corner( row_, col_ ) )
        {
        case TOP_RIGHT_CORNER:

            // 'plus' neighbours
            fuse_depth( row_+1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
            fuse_depth( row_, col_-1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

            // 'cross' neighbours
            fuse_depth( row_+1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
            break;

        case BOTTOM_RIGHT_CORNER:

            // 'plus' neighbours
            fuse_depth( row_-1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
            fuse_depth( row_, col_-1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

            // 'cross' neighbours
            fuse_depth( row_-1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
            break;

        case BOTTOM_LEFT_CORNER:

            // 'plus' neighbours
            fuse_depth( row_-1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
            fuse_depth( row_, col_+1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

            // 'cross' neighbours
            fuse_depth( row_-1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
            break;

        case TOP_LEFT_CORNER:

            // 'plus' neighbours
            fuse_depth( row_+1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
            fuse_depth( row_, col_+1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

            // 'cross' neighbours
            fuse_depth( row_+1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
            break;

        case NO_CORNER:
            switch( is_pixel_on_border( row_, col_ ) )
            {
            case TOP_BORDER:

                // 'plus' neighbours
                fuse_depth( row_+1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_-1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_+1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

                // 'cross' neighbours
                fuse_depth( row_+1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                break;

            case RIGHT_BORDER:

                // 'plus' neighbours
                fuse_depth( row_-1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_-1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

                // 'cross' neighbours
                fuse_depth( row_-1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                break;

            case BOTTOM_BORDER:

                // 'plus' neighbours
                fuse_depth( row_-1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_-1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_+1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

                // 'cross' neighbours
                fuse_depth( row_-1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_-1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                break;

            case LEFT_BORDER:

                // 'plus' neighbours
                fuse_depth( row_-1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_+1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

                // 'cross' neighbours
                fuse_depth( row_-1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                break;

            case NO_BORDER:

                // 'plus' neighbours
                fuse_depth( row_-1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_-1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_, col_+1, plus_neighbours_weight, depth_, flag_only_empty_neighbours );

                // 'cross' neighbours
                fuse_depth( row_-1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_-1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_-1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                fuse_depth( row_+1, col_+1, cross_neighbours_weight, depth_, flag_only_empty_neighbours );
                break;
            }
        }
    }
    //--------------------------------------------------------------------------

    return true;
}

//==============================================================================
//==============================================================================

cv::Mat
FuseDepthData::get_target_image() const
{
    return target_image;
}

//==============================================================================
//==============================================================================

cv::Mat
FuseDepthData::get_total_weight() const
{
    return total_weight;
}

//==============================================================================
//==============================================================================

cv::Mat
FuseDepthData::get_last_updated_factor() const
{
    return last_updated_factor;
}

//==============================================================================
//==============================================================================

bool
FuseDepthData::get_flag_criterion_factor() const
{
    return flag_criterion_factor;
}

//==============================================================================
//==============================================================================

float
FuseDepthData::get_tolerance() const
{
    return tolerance;
}

//==============================================================================
//==============================================================================

int
FuseDepthData::get_plus_neighbours_weight() const
{
    return plus_neighbours_weight;
}

//==============================================================================
//==============================================================================

int
FuseDepthData::get_cross_neighbours_weight() const
{
    return cross_neighbours_weight;
}

//==============================================================================
//==============================================================================

int
FuseDepthData::get_direct_pixel_weight() const
{
    return direct_pixel_weight;
}

//==============================================================================
//==============================================================================

bool
FuseDepthData::get_flag_affect_neighbours() const
{
    return flag_affect_neighbours;
}

//==============================================================================
//==============================================================================

bool
FuseDepthData::get_flag_only_empty_neighbours() const
{
    return flag_only_empty_neighbours;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_target_image( const cv::Mat &image_ )
{
    // target image
    target_image.release();
    image_.copyTo( target_image );

    // and accordingly, the relateds
    total_weight.release();
    total_weight = cv::Mat::zeros( target_image.rows, target_image.cols, CV_32SC1 );

    last_updated_factor.release();
    last_updated_factor = cv::Mat::zeros( target_image.rows, target_image.cols, CV_32FC1 );
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_flag_criterion_factor( bool flag_ )
{
    flag_criterion_factor = flag_;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_tolerance( float value_ )
{
    tolerance = value_;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_plus_neighbours_weight( int weight_ )
{
    plus_neighbours_weight = weight_;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_cross_neighbours_weight( int weight_ )
{
    cross_neighbours_weight = weight_;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_direct_pixel_weight( int weight_ )
{
    direct_pixel_weight = weight_;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_flag_affect_neighbours( bool flag_ )
{
    flag_affect_neighbours = flag_;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::set_flag_only_empty_neighbours( bool flag_ )
{
    flag_only_empty_neighbours = flag_;
}

//==============================================================================
//==============================================================================

FuseDepthData::WHICH_BORDER
FuseDepthData::is_pixel_on_border( int row_, int col_ ) const
{
    if ( target_image.empty() )
    {
        // BADAN ToDo throw an exception
        return NO_BORDER;
    }

    // excluding corners
    if ( NO_CORNER != is_pixel_on_corner( row_, col_) )
    {
        return NO_BORDER;
    }
    if ( 0 == row_ ) return TOP_BORDER;
    if ( target_image.rows-1 == row_) return BOTTOM_BORDER;
    if ( 0 == col_ ) return LEFT_BORDER;
    if ( target_image.cols-1 == col_ ) return RIGHT_BORDER;

    //else
    return NO_BORDER;
}

//==============================================================================
//==============================================================================

FuseDepthData::WHICH_CORNER
FuseDepthData::is_pixel_on_corner( int row_, int col_ ) const
{
    if ( target_image.empty() )
    {
        // BADAN ToDo throw an exception
        return NO_CORNER;
    }

    // top_left
    if ( 0 == row_ || 0 == col_ )
        return TOP_LEFT_CORNER;

    // top_right
    if ( 0 == row_ || target_image.cols-1 == col_ )
        return TOP_RIGHT_CORNER;

    // bottom_left
    if ( target_image.rows-1 == row_ || 0 == col_ )
        return BOTTOM_LEFT_CORNER;

    // bottom-right
    if ( target_image.rows-1 == row_ || target_image.cols-1 == col_ )
        return BOTTOM_RIGHT_CORNER;

    // else
    return NO_CORNER;
}

//==============================================================================
//==============================================================================

bool
FuseDepthData::is_first_update( int row_, int col_ ) const
{
    if ( total_weight.empty() )
    {
        // BADAN ToDo throw an exception
        return false;
    }

    return (0==total_weight.at<int32_t>(row_, col_));
}

//==============================================================================
//==============================================================================

FuseDepthData::NEW_PIXEL_STATUS
FuseDepthData::get_new_pixel_status( int row_, int col_, float new_factor_ )
{
    if ( last_updated_factor.empty() )
    {
        // BADAN ToDo throw an exception
        return UNKNOWN;
    }

    // always replace it for the first time
    if ( 0 == last_updated_factor.at<float>( row_, col_ ) )
    {
        return REPLACE;
    }

    // recent value
    float recent_factor = last_updated_factor.at<float>( row_, col_ );

    // criterion flag
    if ( flag_criterion_factor ) // ascending, keeps smallers
    {
        if ( new_factor_ > (1.0+tolerance)*recent_factor )
        {
            return DISCARD;
        }
        if ( new_factor_ < (1.0-tolerance)*recent_factor )
        {
            return REPLACE;
        }
        // else
        return INTEGRATE;
    }
    else // descending, keeps largers
    {
        if ( new_factor_ < (1.0-tolerance)*recent_factor )
        {
            return DISCARD;
        }
        if ( new_factor_ > (1.0+tolerance)*recent_factor )
        {
            return REPLACE;
        }
        // else
        return INTEGRATE;
    }
}

//==============================================================================
//==============================================================================

void
FuseDepthData::update_last_updated_factor( int row_, int col_, float new_factor_ )
{
    if ( last_updated_factor.empty() )
    {
        // BADAN ToDo throw an exception
        return;
    }

    // always update the factor for the first time
    if ( 0 == last_updated_factor.at<float>( row_, col_ ) )
    {
        last_updated_factor.at<float>( row_, col_ ) = new_factor_;
        return;
    }

    // recent value
    float recent_factor = last_updated_factor.at<float>( row_, col_ );

    // criterion flag
    if ( flag_criterion_factor ) // ascending, keeps smallers
    {
        if ( new_factor_ < recent_factor )
        {
            last_updated_factor.at<float>( row_, col_ ) = new_factor_;
        }
    }
    else // descending, keeps largers
    {
        if ( new_factor_ > recent_factor )
        {
            last_updated_factor.at<float>( row_, col_ ) = new_factor_;
        }
    }
}

//==============================================================================
//==============================================================================

void
FuseDepthData::fuse_depth(int row_, int col_, int new_weight_
                          , const uint16_t new_depth_, bool flag_only_empty )
{
    // BADAN toDo check target data

    // check the flag
    if ( flag_only_empty
         && 0 != target_image.at<uint16_t>( row_, col_ ) )
    {
        return;
    }

    // weight
    int old_weight = total_weight.at<int32_t>( row_, col_ );
    int accumulated_weight = old_weight + new_weight_;
    total_weight.at<int32_t>( row_, col_ ) = accumulated_weight;

    // depth
    uint16_t old_depth = target_image.at<uint16_t>( row_, col_ );
    int fused_depth = ((new_depth_ * new_weight_) + (old_depth * old_weight))
            /accumulated_weight;

    target_image.at<uint16_t>( row_, col_ ) = fused_depth;
}

//==============================================================================
//==============================================================================

void
FuseDepthData::replace_depth(int row_, int col_, int new_weight_
                          , uint16_t new_depth_, bool flag_only_empty )
{
    // BADAN toDo check target data

    // check the flag
    if ( flag_only_empty
         && 0 != target_image.at<uint16_t>( row_, col_ ) )
    {
        return;
    }

    // update the color
    target_image.at<uint16_t>( row_, col_ ) = new_depth_;

    // update the weight
    total_weight.at<int32_t>( row_, col_ ) = new_weight_;
}

//==============================================================================
//==============================================================================


} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
