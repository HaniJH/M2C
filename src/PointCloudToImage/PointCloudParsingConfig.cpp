
// *****************************************************************************
// *********************         H E A D E R S       ***************************
// *****************************************************************************

#include "PointCloudToImage/PointCloudParsingConfig.hpp"

namespace hjh {
namespace pcd2img {

// *****************************************************************************
// *********************    D E F I N I T I O N S    ***************************
// *****************************************************************************

const cv::Vec3b ZERO_COLOR(0,0,0);

//==============================================================================
//==============================================================================

//==============================================================================
//==============================================================================

PointCloudParsingConfig::PointCloudParsingConfig()
    : pcd_to_img_cfg( PointCloudToImageConfig() )
    , horizontal_overlap( 0.5 ) // 50%
    , vertical_overlap( 0.5 )   // 50%
    , max_horizontal_FOV( 360 ) // degree
    , min_horizontal_FOV( 0 )   // degree
    , max_vertical_FOV( 90 )    // degree
    , min_vertical_FOV( -90 )   // degree
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudParsingConfig::PointCloudParsingConfig( PointCloudToImageConfig cfg_
                                                 , double h_overlap_, double v_overlap
                                                 , double max_hfov_, double min_hfov_
                                                 , double max_vfov_, double min_vfov_ )
    : pcd_to_img_cfg( cfg_ )
    , horizontal_overlap( h_overlap_ )  // %
    , vertical_overlap( v_overlap )     // %
    , max_horizontal_FOV( max_hfov_ )   // degree
    , min_horizontal_FOV( min_hfov_ )   // degree
    , max_vertical_FOV( max_vfov_ )     // degree
    , min_vertical_FOV( min_vfov_ )     // degree
{
    // empty
}

//==============================================================================
//==============================================================================

PointCloudParsingConfig::~PointCloudParsingConfig()
{
    // empty
}

//==============================================================================
//==============================================================================

std::ostream&
operator<<(std::ostream& os, const PointCloudParsingConfig& obj)
{
    os
            << "    pcd_to_img_cfg : \n" << obj.pcd_to_img_cfg
            << "\n horizontal_overlap : " << obj.horizontal_overlap
            << "\n   vertical_overlap : " << obj.vertical_overlap
            << "\n max_horizontal_FOV : " << obj.max_horizontal_FOV
            << "\n min_horizontal_FOV : " << obj.min_horizontal_FOV
            << "\n   max_vertical_FOV : " << obj.max_vertical_FOV
            << "\n   min_vertical_FOV : " << obj.min_vertical_FOV
            << "\nvirtual-camera pose"
            << "\n   - is valid:       " << (obj.virtual_camera_pose.isValid ? "YES" : "NO" )
            << "\n   - translation:    " << obj.virtual_camera_pose.translation
            << "\n   - rotaion:        " << obj.virtual_camera_pose.rotation
          ;
    return os;
}

//==============================================================================
//==============================================================================

std::vector<double>
PointCloudParsingConfig::get_horizontal_steps() const
{
    std::vector<double> vec;

    if ( horizontal_overlap < 0 and horizontal_overlap >= 1 )
    {
        // BADAN todo throw an exception
        return vec;
    }

    double init_fov = min_horizontal_FOV;

    while ( init_fov <= max_horizontal_FOV )
    {
        // push in the vector
        vec.push_back( init_fov );

        // the next step
        init_fov += ( 1 - horizontal_overlap ) * pcd_to_img_cfg.horizontal_FOV;
    }

    return vec;
}

//==============================================================================
//==============================================================================

std::vector<double>
PointCloudParsingConfig::get_vertical_steps() const
{
    std::vector<double> vec;

    if ( vertical_overlap < 0 and vertical_overlap >= 1 )
    {
        // BADAN todo throw an exception
        return vec;
    }

    double init_fov = min_vertical_FOV;

    while ( init_fov <= max_vertical_FOV )
    {
        // push in the vector
        vec.push_back( init_fov );

        // the next step
        init_fov += ( 1 - vertical_overlap ) * pcd_to_img_cfg.vertical_FOV;
    }

    return vec;
}

//==============================================================================
//==============================================================================


} // namespace pcd2img
} // namespace hjh

// *****************************************************************************
// *********************    E N D   O F   F I L E    ***************************
// *****************************************************************************
